﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGestioneLetto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGestioneLetto))
        Me.ChkListElencoLetti = New System.Windows.Forms.CheckedListBox()
        Me.cmdControlla = New System.Windows.Forms.Button()
        Me.ChkCorreggi = New System.Windows.Forms.CheckBox()
        Me.TxtLog = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'ChkListElencoLetti
        '
        resources.ApplyResources(Me.ChkListElencoLetti, "ChkListElencoLetti")
        Me.ChkListElencoLetti.FormattingEnabled = True
        Me.ChkListElencoLetti.Name = "ChkListElencoLetti"
        '
        'cmdControlla
        '
        resources.ApplyResources(Me.cmdControlla, "cmdControlla")
        Me.cmdControlla.Name = "cmdControlla"
        Me.cmdControlla.UseVisualStyleBackColor = True
        '
        'ChkCorreggi
        '
        resources.ApplyResources(Me.ChkCorreggi, "ChkCorreggi")
        Me.ChkCorreggi.Name = "ChkCorreggi"
        Me.ChkCorreggi.UseVisualStyleBackColor = True
        '
        'TxtLog
        '
        resources.ApplyResources(Me.TxtLog, "TxtLog")
        Me.TxtLog.Name = "TxtLog"
        '
        'FrmGestioneLetto
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TxtLog)
        Me.Controls.Add(Me.ChkCorreggi)
        Me.Controls.Add(Me.cmdControlla)
        Me.Controls.Add(Me.ChkListElencoLetti)
        Me.Name = "FrmGestioneLetto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChkListElencoLetti As System.Windows.Forms.CheckedListBox
    Friend WithEvents cmdControlla As System.Windows.Forms.Button
    Friend WithEvents ChkCorreggi As System.Windows.Forms.CheckBox
    Friend WithEvents TxtLog As System.Windows.Forms.TextBox
End Class
