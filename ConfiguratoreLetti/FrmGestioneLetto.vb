﻿Public Class FrmGestioneLetto

    Private Sub FrmGestioneLetto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim RsLetti As New ADODB.Recordset
        Dim StrSql As String

        OpenConnLettiRead()

        StrSql = "SELECT * FROM SCL_ScegliIlTuoLetto "
        RsLetti = dbConnConfLettiRead.Execute(StrSql)
        While Not RsLetti.EOF
            ChkListElencoLetti.Items.Add(RsLetti("SCL_Id").Value.ToString & "_" & RsLetti("SCL_Nome").Value.ToString & " " & RsLetti("SCL_SottoNome_it").Value.ToString, False)
            RsLetti.MoveNext()
        End While

    End Sub

    Private Sub cmdControlla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdControlla.Click
        Dim Rs, RsVariantiMis, RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim StrFiltroVariante As String
        Dim StrMisuraCodificata As String
        Dim IntCorreggiErrori As Integer
        Dim ArrSCL_Letto As Array

        IntCorreggiErrori = ChkCorreggi.Checked
        TxtLog.Text = ""
        StrFiltroVariante = ""

        OpenConnLettiRead()

        For Each SCL_Item In ChkListElencoLetti.SelectedItems

            StrSql = "SELECT DISTINCT SCL_VAL_Scegli_Varianti.VAL_Codice FROM SCL_ScegliIlTuoLetto INNER JOIN SCL_VAL_Scegli_Varianti ON SCL_ScegliIlTuoLetto.SCL_Id = SCL_VAL_Scegli_Varianti.SCL_Id "
            StrSql = StrSql & " WHERE SCL_Online >= 1 "
            ArrSCL_Letto = Split(SCL_Item, "_")
            StrSql = StrSql & " and SCL_ScegliIlTuoLetto.SCL_Id = " & ArrSCL_Letto(0).ToString
            'RsTemp = dbConnConfLettiRead.Execute(StrSql)
            RsTemp.Open(StrSql, dbConnConfLettiRead, ADODB.CursorTypeEnum.adOpenStatic)
            While Not RsTemp.EOF
                StrFiltroVariante = StrFiltroVariante & "'" & RsTemp("VAL_Codice").Value.ToString & "',"
                RsTemp.MoveNext()
            End While
            StrFiltroVariante = StrFiltroVariante.Substring(0, Len(StrFiltroVariante) - 1)

            'StrFiltroVariante = "'LGA9','LGA9'"
            StrSql = "SELECT * From VAL_Varianti_Letti Where (VAL_Online >= 1) "
            If StrFiltroVariante <> "" Then
                StrSql = StrSql & " AND VAL_CODICE IN (" & StrFiltroVariante & ") "
            End If
            StrSql = StrSql & " ORDER BY LET_Id"

            Rs = dbConnConfLettiRead.Execute(StrSql)
            While Not Rs.EOF
                TxtLog.Text = TxtLog.Text & vbCrLf & SCL_Item.ToString & " " & Rs("VAL_CODICE").Value.ToString & vbCrLf & vbCrLf

                StrSql = "SELECT MisureListino.*, TIM_MisureTipologiaLetti.*, VMI_Varianti_Misure.VAL_Codice as CodiceValControllo, VMI_Varianti_Misure.TIM_MisuraSpeciale AS MisuraSpecialeOld, VMI_Varianti_Misure.TIM_idPiazza  AS MisuraidPiazzaOld  FROM "
                StrSql = StrSql & " (SELECT DISTINCT VAL_CODICE, Codice_misura, Descrizione_misure From LISTINO_M WHERE (VAL_CODICE = '" & Rs("VAL_CODICE").Value.ToString & "')) MisureListino INNER JOIN TIM_MisureTipologiaLetti ON MisureListino.Codice_misura COLLATE Latin1_General_CI_AS = TIM_MisureTipologiaLetti.TIM_CodBaan LEFT OUTER JOIN VMI_Varianti_Misure ON MisureListino.VAL_CODICE = VMI_Varianti_Misure.VAL_Codice COLLATE Latin1_General_CI_AS AND MisureListino.Codice_misura = VMI_Varianti_Misure.VMI_CodiceMisuraBaan COLLATE Latin1_General_CI_AS"

                'StrSql = StrSql & " (SELECT DISTINCT VAL_CODICE, Codice_misura, Descrizione_misure FROM dbo.LISTINO_M WHERE (VAL_CODICE = '" & Rs("VAL_CODICE").Value.ToString & "')) AS MisureListino INNER JOIN TIM_MisureTipologiaLetti ON MisureListino.Codice_misura COLLATE Latin1_General_CI_AS = TIM_MisureTipologiaLetti.TIM_CodBaan AND MisureListino.Descrizione_misure = TIM_MisureTipologiaLetti.VMI_DescrizioneMisura LEFT OUTER JOIN VMI_Varianti_Misure ON MisureListino.VAL_CODICE = dbo.VMI_Varianti_Misure.VAL_Codice COLLATE Latin1_General_CI_AS AND MisureListino.Codice_misura = dbo.VMI_Varianti_Misure.VMI_CodiceMisuraBaan COLLATE Latin1_General_CI_AS"

                StrSql = StrSql & " ORDER BY TIM_MisureTipologiaLetti.VMI_DescrizioneMisura"

                RsVariantiMis = dbConnConfLettiRead.Execute(StrSql)
                While Not RsVariantiMis.EOF

                    If Trim("" & RsVariantiMis("CodiceValControllo").Value.ToString) = "" Then
                        TxtLog.Text = TxtLog.Text & RsVariantiMis("VAL_CODICE").Value.ToString & " - " & RsVariantiMis("Codice_misura").Value.ToString & " - " & RsVariantiMis("Descrizione_misure").Value.ToString & " - MANCANTE " & vbCrLf

                        If IntCorreggiErrori = -1 Then
                            Select Case RsVariantiMis("TIM_MisuraSpeciale").Value.ToString
                                Case Is <= 1
                                    StrSql = "INSERT INTO VMI_Varianti_Misure (VAL_Codice, VMI_CodiceMisura, VMI_CodiceMisuraBaan, VMI_DescrizioneMisura, VMI_IngombroMisura, TIM_idPiazza, TIM_MisuraSpeciale, TIM_MisuraSpecialePrezzo) "
                                    StrSql = StrSql & " VALUES ('" & RsVariantiMis("VAL_CODICE").Value.ToString & "', '" & Trim(RsVariantiMis("Codice_misura").Value.ToString) & "', '" & Trim(RsVariantiMis("Codice_misura").Value.ToString) & "', '" & Trim(RsVariantiMis("VMI_DescrizioneMisura").Value.ToString) & "', null, " & RsVariantiMis("TIM_idPiazza").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpeciale").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpecialePrezzo").Value.ToString & ")"
                                    OpenConnLettiWrite()
                                    dbConnConfLettiWrite.Execute(StrSql)

                                Case 3
                                    Select Case Trim(RsVariantiMis("TIM_CodBaan").Value.ToString)
                                        Case "683"
                                            StrMisuraCodificata = "682"
                                        Case "687"
                                            StrMisuraCodificata = "686"
                                    End Select

                                    StrSql = "SELECT * From VMI_Varianti_Misure WHERE VAL_Codice = '" & RsVariantiMis("VAL_CODICE").Value.ToString & "' AND VMI_CodiceMisura = '" & StrMisuraCodificata & "' "
                                    RsTemp = dbConnConfLettiRead.Execute(StrSql)
                                    If RsTemp.BOF = True And RsTemp.EOF = True Then
                                        'NON TROVO LA MISURA CORRETTA VA INSERITA E MESSA NELLA TABELLA DELLE DECODIFICHE
                                        StrSql = "INSERT INTO VMI_Varianti_Misure (VAL_Codice, VMI_CodiceMisuraBaan, VMI_CodiceMisura, VMI_DescrizioneMisura, VMI_IngombroMisura, TIM_idPiazza, TIM_MisuraSpeciale, TIM_MisuraSpecialePrezzo) "
                                        StrSql = StrSql & " VALUES ('" & RsVariantiMis("VAL_CODICE").Value.ToString & "', '" & Trim(RsVariantiMis("Codice_misura").Value.ToString) & "', '" & StrMisuraCodificata & "', '" & RsVariantiMis("VMI_DescrizioneMisura").Value.ToString & "', null, " & RsVariantiMis("TIM_idPiazza").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpeciale").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpecialePrezzo").Value.ToString & ")"
                                        OpenConnLettiWrite()
                                        dbConnConfLettiWrite.Execute(StrSql)

                                        '                                StrSql = "DELETE From CONRM_ConfigurazioneRicodificaMisure WHERE VAL_codice = '" & RsVariantiMis("VAL_CODICE") & "' AND TIM_CodBaanVisualizzato =  '" & StrMisuraCodificata & "' AND TIM_CodBaanCorretto = '" & StrMisuraCodificata & "' "
                                        '                                dbConn.Execute (StrSql)
                                        '
                                        '                                StrSql = "INSERT INTO CONRM_ConfigurazioneRicodificaMisure (VAL_codice, TIM_CodBaanVisualizzato, TIM_CodBaanCorretto)  "
                                        '                                StrSql = StrSql & " VALUES ('" & RsVariantiMis("VAL_CODICE") & "', '" & StrMisuraCodificata & "', '" & RsVariantiMis("Codice_misura") & "') "
                                        '                                dbConn.Execute (StrSql)
                                        ' Else
                                        'MsgBox(" VERIFICARE ")
                                    End If

                            End Select
                        End If
                    End If

                    If Trim("" & RsVariantiMis("TIM_MisuraSpeciale").Value.ToString) <> Trim("" & RsVariantiMis("MisuraSpecialeOld").Value.ToString) Then
                        Select Case Trim(RsVariantiMis("TIM_CodBaan").Value.ToString)
                            Case "683"
                                StrMisuraCodificata = "682"
                            Case "687"
                                StrMisuraCodificata = "686"
                            Case Else
                                If Trim(RsVariantiMis("TIM_CodBaan").Value.ToString) Mod 2 = 0 Then
                                    StrMisuraCodificata = Trim(RsVariantiMis("TIM_CodBaan").Value.ToString)
                                Else
                                    MsgBox(" VERIFICARE ")
                                End If
                        End Select

                        TxtLog.Text = TxtLog.Text & RsVariantiMis("VAL_CODICE").Value.ToString & " - " & RsVariantiMis("Codice_misura").Value.ToString & " - " & RsVariantiMis("Descrizione_misure").Value.ToString & " - CODICE MISURA ERRATA " & vbCrLf

                        If IntCorreggiErrori = -1 Then
                            'NON TROVO LA MISURA CORRETTA VA INSERITA E MESSA NELLA TABELLA DELLE DECODIFICHE
                            StrSql = "UPDATE VMI_Varianti_Misure SET  VMI_CodiceMisura = '" & StrMisuraCodificata & "', TIM_MisuraSpeciale = " & RsVariantiMis("TIM_MisuraSpeciale").Value.ToString & ", TIM_MisuraSpecialePrezzo = " & RsVariantiMis("TIM_MisuraSpecialePrezzo").Value.ToString & " "
                            StrSql = StrSql & " WHERE VAL_codice = '" & RsVariantiMis("VAL_CODICE").Value.ToString & "' AND VMI_CodiceMisura = '" & RsVariantiMis("Codice_misura").Value.ToString & "' AND VMI_CodiceMisuraBaan = '" & RsVariantiMis("TIM_CodBaan").Value.ToString & "' "

                            OpenConnLettiWrite()
                            dbConnConfLettiWrite.Execute(StrSql)

                        End If

                    End If
                    If Trim("" & RsVariantiMis("TIM_idPiazza").Value.ToString) <> Trim("" & RsVariantiMis("MisuraidPiazzaOld").Value.ToString) Then
                        TxtLog.Text = TxtLog.Text & RsVariantiMis("VAL_CODICE").Value.ToString & " - " & RsVariantiMis("Codice_misura").Value.ToString & " - " & RsVariantiMis("Descrizione_misure").Value.ToString & " - " & RsVariantiMis("TIM_idPiazza").Value.ToString & " - CODICE TIM_idPiazza ERRATA " & vbCrLf

                    End If

                    RsVariantiMis.MoveNext()
                End While

                Rs.MoveNext()
            End While
            Rs = Nothing
        Next
    End Sub
End Class