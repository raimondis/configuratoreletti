﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDistinta
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox = New System.Windows.Forms.TextBox()
        Me.cmdEsegui = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TextBox
        '
        Me.TextBox.Location = New System.Drawing.Point(12, 12)
        Me.TextBox.Multiline = True
        Me.TextBox.Name = "TextBox"
        Me.TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox.Size = New System.Drawing.Size(1474, 757)
        Me.TextBox.TabIndex = 0
        Me.TextBox.WordWrap = False
        '
        'cmdEsegui
        '
        Me.cmdEsegui.Location = New System.Drawing.Point(1536, 681)
        Me.cmdEsegui.Name = "cmdEsegui"
        Me.cmdEsegui.Size = New System.Drawing.Size(375, 88)
        Me.cmdEsegui.TabIndex = 1
        Me.cmdEsegui.Text = "Carica"
        Me.cmdEsegui.UseVisualStyleBackColor = True
        '
        'FrmDistinta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(16.0!, 31.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1945, 781)
        Me.Controls.Add(Me.cmdEsegui)
        Me.Controls.Add(Me.TextBox)
        Me.Name = "FrmDistinta"
        Me.Text = "Gestione Distinta"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox As TextBox
    Friend WithEvents cmdEsegui As Button
End Class
