﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGruppiTessuto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CmbElencoGruppiTess = New System.Windows.Forms.ComboBox()
        Me.CmbCatModelloLetto = New System.Windows.Forms.ComboBox()
        Me.FlouWebLettiDataSet = New ConfiguratoreLetti.FlouWebLettiDataSet()
        Me.SCLScegliIlTuoLettoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SCL_ScegliIlTuoLettoTableAdapter = New ConfiguratoreLetti.FlouWebLettiDataSetTableAdapters.SCL_ScegliIlTuoLettoTableAdapter()
        Me.CmbLetti = New System.Windows.Forms.ComboBox()
        CType(Me.FlouWebLettiDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SCLScegliIlTuoLettoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CmbElencoGruppiTess
        '
        Me.CmbElencoGruppiTess.FormattingEnabled = True
        Me.CmbElencoGruppiTess.Location = New System.Drawing.Point(12, 12)
        Me.CmbElencoGruppiTess.Name = "CmbElencoGruppiTess"
        Me.CmbElencoGruppiTess.Size = New System.Drawing.Size(194, 21)
        Me.CmbElencoGruppiTess.TabIndex = 0
        '
        'CmbCatModelloLetto
        '
        Me.CmbCatModelloLetto.FormattingEnabled = True
        Me.CmbCatModelloLetto.Location = New System.Drawing.Point(602, 39)
        Me.CmbCatModelloLetto.Name = "CmbCatModelloLetto"
        Me.CmbCatModelloLetto.Size = New System.Drawing.Size(182, 21)
        Me.CmbCatModelloLetto.TabIndex = 2
        '
        'FlouWebLettiDataSet
        '
        Me.FlouWebLettiDataSet.DataSetName = "FlouWebLettiDataSet"
        Me.FlouWebLettiDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SCLScegliIlTuoLettoBindingSource
        '
        Me.SCLScegliIlTuoLettoBindingSource.DataMember = "SCL_ScegliIlTuoLetto"
        Me.SCLScegliIlTuoLettoBindingSource.DataSource = Me.FlouWebLettiDataSet
        '
        'SCL_ScegliIlTuoLettoTableAdapter
        '
        Me.SCL_ScegliIlTuoLettoTableAdapter.ClearBeforeFill = True
        '
        'CmbLetti
        '
        Me.CmbLetti.FormattingEnabled = True
        Me.CmbLetti.Location = New System.Drawing.Point(594, 12)
        Me.CmbLetti.Name = "CmbLetti"
        Me.CmbLetti.Size = New System.Drawing.Size(190, 21)
        Me.CmbLetti.TabIndex = 3
        '
        'FrmGruppiTessuto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(809, 444)
        Me.Controls.Add(Me.CmbLetti)
        Me.Controls.Add(Me.CmbCatModelloLetto)
        Me.Controls.Add(Me.CmbElencoGruppiTess)
        Me.Name = "FrmGruppiTessuto"
        Me.Text = "Form1"
        CType(Me.FlouWebLettiDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SCLScegliIlTuoLettoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CmbElencoGruppiTess As System.Windows.Forms.ComboBox
    Friend WithEvents CmbCatModelloLetto As System.Windows.Forms.ComboBox
    Friend WithEvents FlouWebLettiDataSet As ConfiguratoreLetti.FlouWebLettiDataSet
    Friend WithEvents SCLScegliIlTuoLettoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SCL_ScegliIlTuoLettoTableAdapter As ConfiguratoreLetti.FlouWebLettiDataSetTableAdapters.SCL_ScegliIlTuoLettoTableAdapter
    Friend WithEvents CmbLetti As System.Windows.Forms.ComboBox
End Class
