﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmControlloMisure
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ChkListElencoLetti = New System.Windows.Forms.CheckedListBox()
        Me.cmdControlla = New System.Windows.Forms.Button()
        Me.ChkCorreggi = New System.Windows.Forms.CheckBox()
        Me.TxtLog = New System.Windows.Forms.TextBox()
        Me.BtnCheckCatModello = New System.Windows.Forms.Button()
        Me.BtnCopiaIngombri = New System.Windows.Forms.Button()
        Me.BtnCorreggiIngombri = New System.Windows.Forms.Button()
        Me.cmdNormalizzaMisure = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ChkListElencoLetti
        '
        Me.ChkListElencoLetti.FormattingEnabled = True
        Me.ChkListElencoLetti.Location = New System.Drawing.Point(32, 29)
        Me.ChkListElencoLetti.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.ChkListElencoLetti.Name = "ChkListElencoLetti"
        Me.ChkListElencoLetti.Size = New System.Drawing.Size(807, 1060)
        Me.ChkListElencoLetti.TabIndex = 0
        '
        'cmdControlla
        '
        Me.cmdControlla.Location = New System.Drawing.Point(901, 29)
        Me.cmdControlla.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdControlla.Name = "cmdControlla"
        Me.cmdControlla.Size = New System.Drawing.Size(363, 64)
        Me.cmdControlla.TabIndex = 1
        Me.cmdControlla.Text = "Esegui Controllo Misure"
        Me.cmdControlla.UseVisualStyleBackColor = True
        '
        'ChkCorreggi
        '
        Me.ChkCorreggi.AutoSize = True
        Me.ChkCorreggi.Location = New System.Drawing.Point(1320, 50)
        Me.ChkCorreggi.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.ChkCorreggi.Name = "ChkCorreggi"
        Me.ChkCorreggi.Size = New System.Drawing.Size(162, 36)
        Me.ChkCorreggi.TabIndex = 2
        Me.ChkCorreggi.Text = "Correggi"
        Me.ChkCorreggi.UseVisualStyleBackColor = True
        '
        'TxtLog
        '
        Me.TxtLog.Location = New System.Drawing.Point(901, 165)
        Me.TxtLog.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.TxtLog.Multiline = True
        Me.TxtLog.Name = "TxtLog"
        Me.TxtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TxtLog.Size = New System.Drawing.Size(1607, 941)
        Me.TxtLog.TabIndex = 3
        '
        'BtnCheckCatModello
        '
        Me.BtnCheckCatModello.Location = New System.Drawing.Point(2043, 29)
        Me.BtnCheckCatModello.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.BtnCheckCatModello.Name = "BtnCheckCatModello"
        Me.BtnCheckCatModello.Size = New System.Drawing.Size(472, 57)
        Me.BtnCheckCatModello.TabIndex = 4
        Me.BtnCheckCatModello.Text = "Controlla Categorie Modello "
        Me.BtnCheckCatModello.UseVisualStyleBackColor = True
        '
        'BtnCopiaIngombri
        '
        Me.BtnCopiaIngombri.Location = New System.Drawing.Point(1584, 29)
        Me.BtnCopiaIngombri.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.BtnCopiaIngombri.Name = "BtnCopiaIngombri"
        Me.BtnCopiaIngombri.Size = New System.Drawing.Size(443, 55)
        Me.BtnCopiaIngombri.TabIndex = 5
        Me.BtnCopiaIngombri.Text = "MisureVariantiOpzioni"
        Me.BtnCopiaIngombri.UseVisualStyleBackColor = True
        '
        'BtnCorreggiIngombri
        '
        Me.BtnCorreggiIngombri.Location = New System.Drawing.Point(1584, 98)
        Me.BtnCorreggiIngombri.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.BtnCorreggiIngombri.Name = "BtnCorreggiIngombri"
        Me.BtnCorreggiIngombri.Size = New System.Drawing.Size(443, 52)
        Me.BtnCorreggiIngombri.TabIndex = 6
        Me.BtnCorreggiIngombri.Text = "Correggi Ingombri"
        Me.BtnCorreggiIngombri.UseVisualStyleBackColor = True
        '
        'cmdNormalizzaMisure
        '
        Me.cmdNormalizzaMisure.Location = New System.Drawing.Point(2043, 100)
        Me.cmdNormalizzaMisure.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdNormalizzaMisure.Name = "cmdNormalizzaMisure"
        Me.cmdNormalizzaMisure.Size = New System.Drawing.Size(472, 52)
        Me.cmdNormalizzaMisure.TabIndex = 7
        Me.cmdNormalizzaMisure.Text = "Normalizza Misure"
        Me.cmdNormalizzaMisure.UseVisualStyleBackColor = True
        '
        'FrmControlloMisure
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(16.0!, 31.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(2547, 1142)
        Me.Controls.Add(Me.cmdNormalizzaMisure)
        Me.Controls.Add(Me.BtnCorreggiIngombri)
        Me.Controls.Add(Me.BtnCopiaIngombri)
        Me.Controls.Add(Me.BtnCheckCatModello)
        Me.Controls.Add(Me.TxtLog)
        Me.Controls.Add(Me.ChkCorreggi)
        Me.Controls.Add(Me.cmdControlla)
        Me.Controls.Add(Me.ChkListElencoLetti)
        Me.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.Name = "FrmControlloMisure"
        Me.Text = "Controllo Misure"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChkListElencoLetti As System.Windows.Forms.CheckedListBox
    Friend WithEvents cmdControlla As System.Windows.Forms.Button
    Friend WithEvents ChkCorreggi As System.Windows.Forms.CheckBox
    Friend WithEvents TxtLog As System.Windows.Forms.TextBox
    Friend WithEvents BtnCheckCatModello As System.Windows.Forms.Button
    Friend WithEvents BtnCopiaIngombri As System.Windows.Forms.Button
    Friend WithEvents BtnCorreggiIngombri As System.Windows.Forms.Button
    Friend WithEvents cmdNormalizzaMisure As Button
End Class
