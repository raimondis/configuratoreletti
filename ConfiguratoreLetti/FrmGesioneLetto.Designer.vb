﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmGestioneLetto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.cmdControlla = New System.Windows.Forms.Button()
        Me.ChkListElencoLetti = New System.Windows.Forms.CheckedListBox()
        Me.txtResults = New System.Windows.Forms.TextBox()
        Me.ChkListListini = New System.Windows.Forms.CheckedListBox()
        Me.cmdRicreaFileListino = New System.Windows.Forms.Button()
        Me.cmdAzzeraBiaRiv = New System.Windows.Forms.Button()
        Me.TxtErrori = New System.Windows.Forms.TextBox()
        Me.cmdImpostaAppBiaRiv = New System.Windows.Forms.Button()
        Me.cmdAzzeraBia = New System.Windows.Forms.Button()
        Me.cmdAzzeraRiv = New System.Windows.Forms.Button()
        Me.cmdImpostaAppBia = New System.Windows.Forms.Button()
        Me.cmdImpostaAppRiv = New System.Windows.Forms.Button()
        Me.cmdSelectLettiAll = New System.Windows.Forms.Button()
        Me.cmdUnSelectLettiAll = New System.Windows.Forms.Button()
        Me.btnDelFile = New System.Windows.Forms.Button()
        Me.btnEliminaXmlListino = New System.Windows.Forms.Button()
        Me.CmdGeneraFileRender = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmdControlla
        '
        Me.cmdControlla.Location = New System.Drawing.Point(2536, 29)
        Me.cmdControlla.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdControlla.Name = "cmdControlla"
        Me.cmdControlla.Size = New System.Drawing.Size(363, 119)
        Me.cmdControlla.TabIndex = 3
        Me.cmdControlla.Text = "Esegui Reset Configuratori (www e ba) procedura vecchia"
        Me.cmdControlla.UseVisualStyleBackColor = True
        '
        'ChkListElencoLetti
        '
        Me.ChkListElencoLetti.FormattingEnabled = True
        Me.ChkListElencoLetti.Location = New System.Drawing.Point(32, 29)
        Me.ChkListElencoLetti.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.ChkListElencoLetti.Name = "ChkListElencoLetti"
        Me.ChkListElencoLetti.Size = New System.Drawing.Size(847, 1324)
        Me.ChkListElencoLetti.TabIndex = 2
        '
        'txtResults
        '
        Me.txtResults.Location = New System.Drawing.Point(901, 455)
        Me.txtResults.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.txtResults.Multiline = True
        Me.txtResults.Name = "txtResults"
        Me.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtResults.Size = New System.Drawing.Size(1993, 965)
        Me.txtResults.TabIndex = 4
        '
        'ChkListListini
        '
        Me.ChkListListini.FormattingEnabled = True
        Me.ChkListListini.Location = New System.Drawing.Point(901, 29)
        Me.ChkListListini.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.ChkListListini.Name = "ChkListListini"
        Me.ChkListListini.Size = New System.Drawing.Size(692, 334)
        Me.ChkListListini.TabIndex = 5
        '
        'cmdRicreaFileListino
        '
        Me.cmdRicreaFileListino.Location = New System.Drawing.Point(1664, 29)
        Me.cmdRicreaFileListino.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdRicreaFileListino.Name = "cmdRicreaFileListino"
        Me.cmdRicreaFileListino.Size = New System.Drawing.Size(595, 93)
        Me.cmdRicreaFileListino.TabIndex = 6
        Me.cmdRicreaFileListino.Text = "Azzera xml Letto (WWW- BA - APP)"
        Me.cmdRicreaFileListino.UseVisualStyleBackColor = True
        '
        'cmdAzzeraBiaRiv
        '
        Me.cmdAzzeraBiaRiv.Location = New System.Drawing.Point(1664, 136)
        Me.cmdAzzeraBiaRiv.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdAzzeraBiaRiv.Name = "cmdAzzeraBiaRiv"
        Me.cmdAzzeraBiaRiv.Size = New System.Drawing.Size(312, 74)
        Me.cmdAzzeraBiaRiv.TabIndex = 8
        Me.cmdAzzeraBiaRiv.Text = "AzzeraBiaRiv"
        Me.cmdAzzeraBiaRiv.UseVisualStyleBackColor = True
        '
        'TxtErrori
        '
        Me.TxtErrori.Location = New System.Drawing.Point(2275, 29)
        Me.TxtErrori.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.TxtErrori.Name = "TxtErrori"
        Me.TxtErrori.Size = New System.Drawing.Size(241, 38)
        Me.TxtErrori.TabIndex = 9
        Me.TxtErrori.Text = "0"
        '
        'cmdImpostaAppBiaRiv
        '
        Me.cmdImpostaAppBiaRiv.Location = New System.Drawing.Point(1992, 136)
        Me.cmdImpostaAppBiaRiv.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdImpostaAppBiaRiv.Name = "cmdImpostaAppBiaRiv"
        Me.cmdImpostaAppBiaRiv.Size = New System.Drawing.Size(496, 74)
        Me.cmdImpostaAppBiaRiv.TabIndex = 10
        Me.cmdImpostaAppBiaRiv.Text = "Carica XML e Predisponi App Letti"
        Me.cmdImpostaAppBiaRiv.UseVisualStyleBackColor = True
        '
        'cmdAzzeraBia
        '
        Me.cmdAzzeraBia.Location = New System.Drawing.Point(1664, 224)
        Me.cmdAzzeraBia.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdAzzeraBia.Name = "cmdAzzeraBia"
        Me.cmdAzzeraBia.Size = New System.Drawing.Size(312, 74)
        Me.cmdAzzeraBia.TabIndex = 11
        Me.cmdAzzeraBia.Text = "AzzeraBia"
        Me.cmdAzzeraBia.UseVisualStyleBackColor = True
        '
        'cmdAzzeraRiv
        '
        Me.cmdAzzeraRiv.Location = New System.Drawing.Point(1664, 312)
        Me.cmdAzzeraRiv.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdAzzeraRiv.Name = "cmdAzzeraRiv"
        Me.cmdAzzeraRiv.Size = New System.Drawing.Size(312, 76)
        Me.cmdAzzeraRiv.TabIndex = 12
        Me.cmdAzzeraRiv.Text = "AzzeraRiv"
        Me.cmdAzzeraRiv.UseVisualStyleBackColor = True
        '
        'cmdImpostaAppBia
        '
        Me.cmdImpostaAppBia.Location = New System.Drawing.Point(1992, 224)
        Me.cmdImpostaAppBia.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdImpostaAppBia.Name = "cmdImpostaAppBia"
        Me.cmdImpostaAppBia.Size = New System.Drawing.Size(496, 74)
        Me.cmdImpostaAppBia.TabIndex = 13
        Me.cmdImpostaAppBia.Text = "Carica XML e Predisponi App Letti"
        Me.cmdImpostaAppBia.UseVisualStyleBackColor = True
        '
        'cmdImpostaAppRiv
        '
        Me.cmdImpostaAppRiv.Location = New System.Drawing.Point(1992, 312)
        Me.cmdImpostaAppRiv.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdImpostaAppRiv.Name = "cmdImpostaAppRiv"
        Me.cmdImpostaAppRiv.Size = New System.Drawing.Size(496, 74)
        Me.cmdImpostaAppRiv.TabIndex = 14
        Me.cmdImpostaAppRiv.Text = "Carica XML e Predisponi App Letti"
        Me.cmdImpostaAppRiv.UseVisualStyleBackColor = True
        '
        'cmdSelectLettiAll
        '
        Me.cmdSelectLettiAll.Location = New System.Drawing.Point(32, 1371)
        Me.cmdSelectLettiAll.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdSelectLettiAll.Name = "cmdSelectLettiAll"
        Me.cmdSelectLettiAll.Size = New System.Drawing.Size(243, 55)
        Me.cmdSelectLettiAll.TabIndex = 15
        Me.cmdSelectLettiAll.Text = "Select All"
        Me.cmdSelectLettiAll.UseVisualStyleBackColor = True
        '
        'cmdUnSelectLettiAll
        '
        Me.cmdUnSelectLettiAll.Location = New System.Drawing.Point(643, 1371)
        Me.cmdUnSelectLettiAll.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.cmdUnSelectLettiAll.Name = "cmdUnSelectLettiAll"
        Me.cmdUnSelectLettiAll.Size = New System.Drawing.Size(243, 55)
        Me.cmdUnSelectLettiAll.TabIndex = 16
        Me.cmdUnSelectLettiAll.Text = "UnSelect All"
        Me.cmdUnSelectLettiAll.UseVisualStyleBackColor = True
        '
        'btnDelFile
        '
        Me.btnDelFile.Location = New System.Drawing.Point(2539, 312)
        Me.btnDelFile.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.btnDelFile.Name = "btnDelFile"
        Me.btnDelFile.Size = New System.Drawing.Size(363, 74)
        Me.btnDelFile.TabIndex = 17
        Me.btnDelFile.Text = "Cancella Immagini"
        Me.btnDelFile.UseVisualStyleBackColor = True
        '
        'btnEliminaXmlListino
        '
        Me.btnEliminaXmlListino.Location = New System.Drawing.Point(2539, 205)
        Me.btnEliminaXmlListino.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.btnEliminaXmlListino.Name = "btnEliminaXmlListino"
        Me.btnEliminaXmlListino.Size = New System.Drawing.Size(360, 90)
        Me.btnEliminaXmlListino.TabIndex = 18
        Me.btnEliminaXmlListino.Text = "Cancella XML Fuori Listino"
        Me.btnEliminaXmlListino.UseVisualStyleBackColor = True
        '
        'CmdGeneraFileRender
        '
        Me.CmdGeneraFileRender.Location = New System.Drawing.Point(2963, 29)
        Me.CmdGeneraFileRender.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.CmdGeneraFileRender.Name = "CmdGeneraFileRender"
        Me.CmdGeneraFileRender.Size = New System.Drawing.Size(363, 74)
        Me.CmdGeneraFileRender.TabIndex = 19
        Me.CmdGeneraFileRender.Text = "Genera File Render Rivestimenti"
        Me.CmdGeneraFileRender.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(2963, 205)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(363, 90)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "Cancella Json"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmGestioneLetto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(16.0!, 31.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(3365, 1455)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CmdGeneraFileRender)
        Me.Controls.Add(Me.btnEliminaXmlListino)
        Me.Controls.Add(Me.btnDelFile)
        Me.Controls.Add(Me.cmdUnSelectLettiAll)
        Me.Controls.Add(Me.cmdSelectLettiAll)
        Me.Controls.Add(Me.cmdImpostaAppRiv)
        Me.Controls.Add(Me.cmdImpostaAppBia)
        Me.Controls.Add(Me.cmdAzzeraRiv)
        Me.Controls.Add(Me.cmdAzzeraBia)
        Me.Controls.Add(Me.cmdImpostaAppBiaRiv)
        Me.Controls.Add(Me.TxtErrori)
        Me.Controls.Add(Me.cmdAzzeraBiaRiv)
        Me.Controls.Add(Me.cmdRicreaFileListino)
        Me.Controls.Add(Me.ChkListListini)
        Me.Controls.Add(Me.txtResults)
        Me.Controls.Add(Me.cmdControlla)
        Me.Controls.Add(Me.ChkListElencoLetti)
        Me.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.Name = "FrmGestioneLetto"
        Me.Text = "FrmGestioneLetto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdControlla As System.Windows.Forms.Button
    Friend WithEvents ChkListElencoLetti As System.Windows.Forms.CheckedListBox
    Private WithEvents txtResults As System.Windows.Forms.TextBox
    Friend WithEvents ChkListListini As System.Windows.Forms.CheckedListBox
    Friend WithEvents cmdRicreaFileListino As System.Windows.Forms.Button
    Friend WithEvents cmdAzzeraBiaRiv As System.Windows.Forms.Button
    Friend WithEvents TxtErrori As System.Windows.Forms.TextBox
    Friend WithEvents cmdImpostaAppBiaRiv As System.Windows.Forms.Button
    Friend WithEvents cmdAzzeraBia As System.Windows.Forms.Button
    Friend WithEvents cmdAzzeraRiv As System.Windows.Forms.Button
    Friend WithEvents cmdImpostaAppBia As System.Windows.Forms.Button
    Friend WithEvents cmdImpostaAppRiv As System.Windows.Forms.Button
    Friend WithEvents cmdSelectLettiAll As System.Windows.Forms.Button
    Friend WithEvents cmdUnSelectLettiAll As System.Windows.Forms.Button
    Friend WithEvents btnDelFile As System.Windows.Forms.Button
    Friend WithEvents btnEliminaXmlListino As System.Windows.Forms.Button
    Friend WithEvents CmdGeneraFileRender As System.Windows.Forms.Button
    Friend WithEvents Button1 As Button
End Class
