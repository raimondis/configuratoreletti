﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ScL_ScegliIlTuoLettoTableAdapter1 = New ConfiguratoreLetti.FlouWebLettiDataSetTableAdapters.SCL_ScegliIlTuoLettoTableAdapter()
        Me.BiA_COL_BiancherieColoriTableAdapter1 = New ConfiguratoreLetti.FlouWebLettiDataSetTableAdapters.BIA_COL_BiancherieColoriTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CmdOpenFrmControlloMisure = New System.Windows.Forms.Button()
        Me.CmdFrmGestLetto = New System.Windows.Forms.Button()
        Me.cmbButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ScL_ScegliIlTuoLettoTableAdapter1
        '
        Me.ScL_ScegliIlTuoLettoTableAdapter1.ClearBeforeFill = True
        '
        'BiA_COL_BiancherieColoriTableAdapter1
        '
        Me.BiA_COL_BiancherieColoriTableAdapter1.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(83, 558)
        Me.Button1.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(389, 69)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CmdOpenFrmControlloMisure
        '
        Me.CmdOpenFrmControlloMisure.Location = New System.Drawing.Point(83, 81)
        Me.CmdOpenFrmControlloMisure.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.CmdOpenFrmControlloMisure.Name = "CmdOpenFrmControlloMisure"
        Me.CmdOpenFrmControlloMisure.Size = New System.Drawing.Size(389, 74)
        Me.CmdOpenFrmControlloMisure.TabIndex = 1
        Me.CmdOpenFrmControlloMisure.Text = "Controllo Misure"
        Me.CmdOpenFrmControlloMisure.UseVisualStyleBackColor = True
        '
        'CmdFrmGestLetto
        '
        Me.CmdFrmGestLetto.Location = New System.Drawing.Point(83, 196)
        Me.CmdFrmGestLetto.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.CmdFrmGestLetto.Name = "CmdFrmGestLetto"
        Me.CmdFrmGestLetto.Size = New System.Drawing.Size(389, 107)
        Me.CmdFrmGestLetto.TabIndex = 2
        Me.CmdFrmGestLetto.Text = "Gestione Aggiornamento XML"
        Me.CmdFrmGestLetto.UseVisualStyleBackColor = True
        '
        'cmbButton
        '
        Me.cmbButton.Location = New System.Drawing.Point(601, 491)
        Me.cmbButton.Name = "cmbButton"
        Me.cmbButton.Size = New System.Drawing.Size(234, 136)
        Me.cmbButton.TabIndex = 3
        Me.cmbButton.Text = "Button2"
        Me.cmbButton.UseVisualStyleBackColor = True
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(16.0!, 31.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(939, 794)
        Me.Controls.Add(Me.cmbButton)
        Me.Controls.Add(Me.CmdFrmGestLetto)
        Me.Controls.Add(Me.CmdOpenFrmControlloMisure)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(8, 7, 8, 7)
        Me.Name = "FrmMain"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ScL_ScegliIlTuoLettoTableAdapter1 As ConfiguratoreLetti.FlouWebLettiDataSetTableAdapters.SCL_ScegliIlTuoLettoTableAdapter
    Friend WithEvents BiA_COL_BiancherieColoriTableAdapter1 As ConfiguratoreLetti.FlouWebLettiDataSetTableAdapters.BIA_COL_BiancherieColoriTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CmdOpenFrmControlloMisure As System.Windows.Forms.Button
    Friend WithEvents CmdFrmGestLetto As System.Windows.Forms.Button
    Friend WithEvents cmbButton As Button
End Class
