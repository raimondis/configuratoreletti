﻿Public Class FrmControlloMisure

    Private Sub FrmControlloMisure_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim RsLetti As New ADODB.Recordset
        Dim StrSql As String

        OpenConnLettiRead()

        StrSql = "SELECT * FROM SCL_ScegliIlTuoLetto WHERE SCL_Online >=1 order by SCL_nome"
        RsLetti = dbConnConfLettiRead.Execute(StrSql)
        While Not RsLetti.EOF
            ChkListElencoLetti.Items.Add(RsLetti("SCL_Id").Value.ToString & "_" & RsLetti("SCL_Nome").Value.ToString & " " & RsLetti("SCL_SottoNome_it").Value.ToString, False)
            RsLetti.MoveNext()
        End While

        TxtLog.Width = Me.Width - TxtLog.Location.X - 30
        TxtLog.Height = Me.Height - TxtLog.Location.Y - 50
        ChkListElencoLetti.Height = Me.Height - TxtLog.Location.Y - 5

    End Sub

    Private Sub cmdControlla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdControlla.Click
        Dim Rs, RsVariantiMis, RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim StrFiltroVariante As String
        Dim StrMisuraCodificata, StrListino As String
        Dim IntCorreggiErrori As Integer
        Dim ArrSCL_Letto As Array
        Dim IntCorretto As Integer

        IntCorreggiErrori = ChkCorreggi.Checked
        TxtLog.Text = ""
        StrFiltroVariante = ""
        IntCorretto = 0
        StrListino = "651"

        OpenConnLettiRead()

        For Each SCL_Item In ChkListElencoLetti.CheckedItems

            OpenConnLettiRead()

            StrSql = "SELECT DISTINCT SCL_VAL_Scegli_Varianti.VAL_Codice FROM SCL_ScegliIlTuoLetto INNER JOIN SCL_VAL_Scegli_Varianti ON SCL_ScegliIlTuoLetto.SCL_Id = SCL_VAL_Scegli_Varianti.SCL_Id "
            StrSql = StrSql & " WHERE SCL_Online >= 1 "
            ArrSCL_Letto = Split(SCL_Item, "_")
            StrSql = StrSql & " and SCL_ScegliIlTuoLetto.SCL_Id = " & ArrSCL_Letto(0).ToString
            RsTemp = dbConnConfLettiRead.Execute(StrSql)
            'RsTemp.Open(StrSql, dbConnConfLettiRead, ADODB.CursorTypeEnum.adOpenStatic)
            StrFiltroVariante = ""
            While Not RsTemp.EOF
                StrFiltroVariante = StrFiltroVariante & "'" & RsTemp("VAL_Codice").Value.ToString & "',"
                RsTemp.MoveNext()
            End While
            StrFiltroVariante = StrFiltroVariante.Substring(0, Len(StrFiltroVariante) - 1)

            'StrFiltroVariante = "'LGA9','LGA9'"
            '  StrSql = "SELECT * From VAL_Varianti_Letti Where (VAL_Online >= 1) "
            '  If StrFiltroVariante <> "" Then
            ' StrSql = StrSql & " AND VAL_CODICE IN (" & StrFiltroVariante & ") "
            ' End If
            'StrSql = StrSql & " ORDER BY LET_Id"

            StrSql = " Select distinct * from ( SELECT VAL_Codice From VAL_Varianti_Letti Where (VAL_Online >= 1)  "
            If StrFiltroVariante <> "" Then
                StrSql = StrSql & " AND VAL_CODICE IN (" & StrFiltroVariante & ") "
            End If
            StrSql = StrSql & " union "

            StrSql = StrSql & " Select VAL_Codice_New FROM dbo.OPZLCM_OpzioniLettiFiltriCatMod WHERE VAL_Codice_New is not null"
            If StrFiltroVariante <> "" Then
                StrSql = StrSql & " AND VAL_CODICE IN (" & StrFiltroVariante & ") "
            End If
            StrSql = StrSql & " ) as a"

            Rs = dbConnConfLettiRead.Execute(StrSql)
            While Not Rs.EOF
                TxtLog.Text = TxtLog.Text & vbCrLf & SCL_Item.ToString & " " & Rs("VAL_CODICE").Value.ToString & vbCrLf & vbCrLf

                StrSql = "SELECT MisureListino.*, TIM_MisureTipologiaLetti.*, VMI_Varianti_Misure.VAL_Codice as CodiceValControllo, VMI_Varianti_Misure.TIM_MisuraSpeciale AS MisuraSpecialeOld, VMI_Varianti_Misure.TIM_idPiazza  AS MisuraidPiazzaOld  FROM "
                StrSql = StrSql & " (SELECT DISTINCT VAL_CODICE, Codice_misura, Descrizione_misure From LISTINO_M WHERE VAL_CODICE = '" & Rs("VAL_CODICE").Value.ToString & "' and t_cpls ='" & StrListino & "' ) MisureListino INNER JOIN TIM_MisureTipologiaLetti ON MisureListino.Codice_misura COLLATE Latin1_General_CI_AS = TIM_MisureTipologiaLetti.TIM_CodBaan LEFT OUTER JOIN VMI_Varianti_Misure ON MisureListino.VAL_CODICE = VMI_Varianti_Misure.VAL_Codice COLLATE Latin1_General_CI_AS AND MisureListino.Codice_misura = VMI_Varianti_Misure.VMI_CodiceMisuraBaan COLLATE Latin1_General_CI_AS"

                'StrSql = StrSql & " (SELECT DISTINCT VAL_CODICE, Codice_misura, Descrizione_misure FROM dbo.LISTINO_M WHERE (VAL_CODICE = '" & Rs("VAL_CODICE").Value.ToString & "')) AS MisureListino INNER JOIN TIM_MisureTipologiaLetti ON MisureListino.Codice_misura COLLATE Latin1_General_CI_AS = TIM_MisureTipologiaLetti.TIM_CodBaan AND MisureListino.Descrizione_misure = TIM_MisureTipologiaLetti.VMI_DescrizioneMisura LEFT OUTER JOIN VMI_Varianti_Misure ON MisureListino.VAL_CODICE = dbo.VMI_Varianti_Misure.VAL_Codice COLLATE Latin1_General_CI_AS AND MisureListino.Codice_misura = dbo.VMI_Varianti_Misure.VMI_CodiceMisuraBaan COLLATE Latin1_General_CI_AS"
                StrSql = StrSql & " WHERE(TIM_MisureTipologiaLetti.TIM_MisuraSpeciale < 3) "
                StrSql = StrSql & " ORDER BY TIM_MisureTipologiaLetti.VMI_DescrizioneMisura"

                RsVariantiMis = dbConnConfLettiRead.Execute(StrSql)
                While Not RsVariantiMis.EOF
                    IntCorretto = 0
                    If Trim("" & RsVariantiMis("CodiceValControllo").Value.ToString) = "" Then
                        TxtLog.Text = TxtLog.Text & RsVariantiMis("VAL_CODICE").Value.ToString & " - " & RsVariantiMis("Codice_misura").Value.ToString & " - " & RsVariantiMis("Descrizione_misure").Value.ToString & " - MANCANTE " & vbCrLf

                        If IntCorreggiErrori = -1 Then
                            Select Case RsVariantiMis("TIM_MisuraSpeciale").Value.ToString
                                Case Is <= 1
                                    StrSql = "INSERT INTO VMI_Varianti_Misure (VAL_Codice, VMI_CodiceMisura, VMI_CodiceMisuraBaan, VMI_DescrizioneMisura, VMI_IngombroMisura, TIM_idPiazza, TIM_MisuraSpeciale, TIM_MisuraSpecialePrezzo) "
                                    StrSql = StrSql & " VALUES ('" & RsVariantiMis("VAL_CODICE").Value.ToString & "', '" & Trim(RsVariantiMis("Codice_misura").Value.ToString) & "', '" & Trim(RsVariantiMis("Codice_misura").Value.ToString) & "', '" & Trim(RsVariantiMis("VMI_DescrizioneMisura").Value.ToString) & "', null, " & RsVariantiMis("TIM_idPiazza").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpeciale").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpecialePrezzo").Value.ToString & ")"
                                    OpenConnLettiWrite()
                                    dbConnConfLettiWrite.Execute(StrSql)
                                Case 2
                                    StrSql = "INSERT INTO VMI_Varianti_Misure (t_cpls,VAL_Codice, VMI_CodiceMisura, VMI_CodiceMisuraBaan, VMI_DescrizioneMisura, VMI_IngombroMisura, TIM_idPiazza, TIM_MisuraSpeciale, TIM_MisuraSpecialePrezzo) "
                                    StrSql = StrSql & " VALUES ('USA','" & RsVariantiMis("VAL_CODICE").Value.ToString & "', '" & Trim(RsVariantiMis("Codice_misura").Value.ToString) & "', '" & Trim(RsVariantiMis("Codice_misura").Value.ToString) & "', '" & Trim(RsVariantiMis("VMI_DescrizioneMisura").Value.ToString) & "', null, " & RsVariantiMis("TIM_idPiazza").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpeciale").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpecialePrezzo").Value.ToString & ")"
                                    OpenConnLettiWrite()
                                    dbConnConfLettiWrite.Execute(StrSql)
                                Case 3
                                    Select Case Trim(RsVariantiMis("TIM_CodBaan").Value.ToString)
                                        Case "683"
                                            StrMisuraCodificata = "682"
                                        Case "687"
                                            StrMisuraCodificata = "686"
                                    End Select

                                    StrSql = "SELECT * From VMI_Varianti_Misure WHERE VAL_Codice = '" & RsVariantiMis("VAL_CODICE").Value.ToString & "' AND VMI_CodiceMisura = '" & StrMisuraCodificata & "' "
                                    RsTemp = dbConnConfLettiRead.Execute(StrSql)
                                    If RsTemp.BOF = True And RsTemp.EOF = True Then
                                        'NON TROVO LA MISURA CORRETTA VA INSERITA E MESSA NELLA TABELLA DELLE DECODIFICHE
                                        StrSql = "INSERT INTO VMI_Varianti_Misure (VAL_Codice, VMI_CodiceMisuraBaan, VMI_CodiceMisura, VMI_DescrizioneMisura, VMI_IngombroMisura, TIM_idPiazza, TIM_MisuraSpeciale, TIM_MisuraSpecialePrezzo) "
                                        StrSql = StrSql & " VALUES ('" & RsVariantiMis("VAL_CODICE").Value.ToString & "', '" & Trim(RsVariantiMis("Codice_misura").Value.ToString) & "', '" & StrMisuraCodificata & "', '" & RsVariantiMis("VMI_DescrizioneMisura").Value.ToString & "', null, " & RsVariantiMis("TIM_idPiazza").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpeciale").Value.ToString & ", " & RsVariantiMis("TIM_MisuraSpecialePrezzo").Value.ToString & ")"
                                        OpenConnLettiWrite()
                                        dbConnConfLettiWrite.Execute(StrSql)

                                        '                                StrSql = "DELETE From CONRM_ConfigurazioneRicodificaMisure WHERE VAL_codice = '" & RsVariantiMis("VAL_CODICE") & "' AND TIM_CodBaanVisualizzato =  '" & StrMisuraCodificata & "' AND TIM_CodBaanCorretto = '" & StrMisuraCodificata & "' "
                                        '                                dbConn.Execute (StrSql)
                                        '
                                        '                                StrSql = "INSERT INTO CONRM_ConfigurazioneRicodificaMisure (VAL_codice, TIM_CodBaanVisualizzato, TIM_CodBaanCorretto)  "
                                        '                                StrSql = StrSql & " VALUES ('" & RsVariantiMis("VAL_CODICE") & "', '" & StrMisuraCodificata & "', '" & RsVariantiMis("Codice_misura") & "') "
                                        '                                dbConn.Execute (StrSql)
                                        ' Else
                                        'MsgBox(" VERIFICARE ")
                                    End If

                            End Select
                            IntCorretto = 1

                        End If
                    End If

                    If Trim("" & RsVariantiMis("TIM_MisuraSpeciale").Value.ToString) <> Trim("" & RsVariantiMis("MisuraSpecialeOld").Value.ToString) Then
                        Select Case Trim(RsVariantiMis("TIM_CodBaan").Value.ToString)
                            Case "683"
                                StrMisuraCodificata = "682"
                            Case "687"
                                StrMisuraCodificata = "686"
                            Case Else
                                If Trim(RsVariantiMis("TIM_CodBaan").Value.ToString) Mod 2 = 0 Then
                                    StrMisuraCodificata = Trim(RsVariantiMis("TIM_CodBaan").Value.ToString)
                                Else
                                    MsgBox(" VERIFICARE ")
                                End If
                        End Select

                        TxtLog.Text = TxtLog.Text & RsVariantiMis("VAL_CODICE").Value.ToString & " - " & RsVariantiMis("Codice_misura").Value.ToString & " - " & RsVariantiMis("Descrizione_misure").Value.ToString & " - TIM_MisuraSpeciale <> da quella in VMI_VARIANTI MISURA " & vbCrLf

                        If IntCorreggiErrori = -1 And IntCorretto = 0 Then
                            'NON TROVO LA MISURA CORRETTA VA INSERITA E MESSA NELLA TABELLA DELLE DECODIFICHE
                            StrSql = "UPDATE VMI_Varianti_Misure SET  VMI_CodiceMisura = '" & StrMisuraCodificata & "', TIM_MisuraSpeciale = " & RsVariantiMis("TIM_MisuraSpeciale").Value.ToString & ", TIM_MisuraSpecialePrezzo = " & RsVariantiMis("TIM_MisuraSpecialePrezzo").Value.ToString & " "
                            StrSql = StrSql & " WHERE VAL_codice = '" & RsVariantiMis("VAL_CODICE").Value.ToString & "' AND VMI_CodiceMisura = '" & RsVariantiMis("Codice_misura").Value.ToString & "' AND VMI_CodiceMisuraBaan = '" & RsVariantiMis("TIM_CodBaan").Value.ToString & "' "

                            OpenConnLettiWrite()
                            dbConnConfLettiWrite.Execute(StrSql)
                            IntCorretto = 1
                        End If

                    End If
                    If Trim("" & RsVariantiMis("TIM_idPiazza").Value.ToString) <> Trim("" & RsVariantiMis("MisuraidPiazzaOld").Value.ToString) Then
                        TxtLog.Text = TxtLog.Text & RsVariantiMis("VAL_CODICE").Value.ToString & " - " & RsVariantiMis("Codice_misura").Value.ToString & " - " & RsVariantiMis("Descrizione_misure").Value.ToString & " - " & RsVariantiMis("TIM_idPiazza").Value.ToString & " - CODICE TIM_idPiazza ERRATA " & vbCrLf
                        If IntCorreggiErrori = -1 And IntCorretto = 0 Then
                            StrSql = "UPDATE VMI_Varianti_Misure SET  TIM_idPiazza = '" & RsVariantiMis("TIM_idPiazza").Value.ToString & "'"
                            StrSql = StrSql & " WHERE VAL_codice = '" & RsVariantiMis("VAL_CODICE").Value.ToString & "' AND VMI_CodiceMisura = '" & RsVariantiMis("Codice_misura").Value.ToString & "' AND VMI_CodiceMisuraBaan = '" & RsVariantiMis("TIM_CodBaan").Value.ToString & "' "

                            OpenConnLettiWrite()
                            dbConnConfLettiWrite.Execute(StrSql)
                        End If
                    End If

                    RsVariantiMis.MoveNext()
                End While

                Rs.MoveNext()
            End While
            Rs = Nothing
            dbConnConfLettiRead.Close()
        Next
    End Sub

    Private Sub FrmControlloMisure_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        TxtLog.Width = Me.Width - TxtLog.Location.X - 30
        TxtLog.Height = Me.Height - TxtLog.Location.Y - 50
        ChkListElencoLetti.Height = Me.Height - TxtLog.Location.Y - 5
    End Sub

    Private Sub BtnCheckCatModello_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCheckCatModello.Click
        Dim RsLetti, RsBaanVarianti, RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim IntCorreggiErrori As Integer
        Dim ArrSCL_Letto As Array

        IntCorreggiErrori = ChkCorreggi.Checked
        TxtLog.Text = ""



        For Each SCL_Item In ChkListElencoLetti.CheckedItems
            ArrSCL_Letto = Split(SCL_Item, "_")
            'StrSql = StrSql & " and SCL_ScegliIlTuoLetto.SCL_Id = " & ArrSCL_Letto(0).ToString

            OpenConnLettiRead()
            StrSql = " SELECT  * FROM  dbo.LET_Letti INNER JOIN dbo.SCL_ScegliIlTuoLetto ON dbo.LET_Letti.LET_Id = dbo.SCL_ScegliIlTuoLetto.LET_Id INNER JOIN dbo.TIM_MisureTipologiaLetti ON dbo.SCL_ScegliIlTuoLetto.SCL_DimensioneLetto = dbo.TIM_MisureTipologiaLetti.VMI_DescrizioneMisura "

            StrSql = StrSql & "  WHERE (NOT (LET_csgs_raggbaan IS NULL)) and SCL_Online >=1 and SCL_ScegliIlTuoLetto.SCL_Id = " & ArrSCL_Letto(0).ToString

            RsLetti.Open(StrSql, dbConnConfLettiRead, ADODB.CursorTypeEnum.adOpenStatic)

            While Not RsLetti.EOF

                TxtLog.Text = TxtLog.Text & RsLetti("LET_csgs_raggbaan").Value.ToString & " " & RsLetti("LET_Nome_IT").Value.ToString & " " & RsLetti("SCL_Id").Value.ToString & " " & RsLetti("VMI_DescrizioneMisura").Value.ToString & vbCrLf

                OpenConnBaan()
                StrSql = " SELECT distinct ttcmcs023400.t_citg, ttcmcs023400.t_dsca, ttcmcs023400.t_taglio, ttcmcs023400.t_finp_c, ttcftc012400.t_gcit, ttcftc012400.t_dsca AS Expr3, ttcftc012400.t_prpg, ttcftc012400.t_sm,  ttcftc012400.t_s1, ttcftc012400.t_s2, ttcftc012400.t_s3 "
                StrSql = StrSql & " FROM ttdsls032400 INNER JOIN ttcmcs023400 INNER JOIN  ttcftc012400 ON ttcmcs023400.t_gcit_c = ttcftc012400.t_gcit ON  LEFT(ttdsls032400.t_item , 4) = ttcmcs023400.t_citg WHERE (ttcmcs023400.t_csgp = '" & RsLetti("LET_csgs_raggbaan").Value.ToString & "') and ttdsls032400.t_cpls = '651' AND  SUBSTRING(ttdsls032400.t_item, 9,3) = '" & RsLetti("TIM_CodBaan").Value.ToString & "'"
                RsBaanVarianti = dbConnBaan.Execute(StrSql)
                If RsBaanVarianti.BOF = True And RsBaanVarianti.EOF = True Then

                    TxtLog.Text = TxtLog.Text & "NESSUNA VARIANTE TROVATA IN BAAN" & vbCrLf

                Else

                    While Not RsBaanVarianti.EOF

                        StrSql = " SELECT * FROM VAL_Varianti_Letti "
                        StrSql = StrSql & "  WHERE VAL_Codice = '" & Trim(RsBaanVarianti("t_citg").Value.ToString) & "'"
                        'OpenConnLettiRead()
                        OpenConnLettiWrite()
                        RsTemp = dbConnConfLettiWrite.Execute(StrSql)
                        ' RsTemp = dbConnConfLettiRead.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            ' non trovata
                            TxtLog.Text = TxtLog.Text & RsBaanVarianti("t_citg").Value.ToString & " - NON TROVATA NEL SITO" & vbCrLf

                            If IntCorreggiErrori = -1 Then
                                StrSql = " INSERT INTO VAL_Varianti_Letti ( LET_Id, VAL_Codice, VAL_Descrizione_it,  BAS_PDR_id, BAS_PDR_id_ba, VAL_SoloMatSingoli, VAL_Online)"
                                StrSql = StrSql & "  VALUES  ( " & RsLetti("LET_Id").Value.ToString & ", '" & Trim(RsBaanVarianti("t_citg").Value.ToString) & "', '" & Replace(RsBaanVarianti("t_dsca").Value.ToString, "'", "") & "',  0, 0,0, 0) "
                                OpenConnLettiWrite()
                                dbConnConfLettiWrite.Execute(StrSql)

                                TxtLog.Text = TxtLog.Text & RsBaanVarianti("t_citg").Value.ToString & " - INSERITA NELLA TABELLA VAL_Varianti_Letti" & vbCrLf

                                StrSql = " SELECT * FROM  dbo.SCL_VAL_Scegli_Varianti"
                                StrSql = StrSql & "  WHERE VAL_Codice = '" & Trim(RsBaanVarianti("t_citg").Value.ToString) & "' AND SCL_Id = " & RsLetti("SCL_Id").Value
                                RsTemp = dbConnConfLettiRead.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    TxtLog.Text = TxtLog.Text & RsBaanVarianti("t_citg").Value.ToString & " - " & RsLetti("SCL_Id").Value.ToString & " - AGGIUNTO TROVATO ABBINAMENTO VAL_CODICE SCL_ID NEL SITO" & vbCrLf
                                    StrSql = " INSERT INTO SCL_VAL_Scegli_Varianti ( VAL_Codice, SCL_Id )"
                                    StrSql = StrSql & "  VALUES  ( '" & Trim(RsBaanVarianti("t_citg").Value.ToString) & "'," & RsLetti("SCL_Id").Value.ToString & ") "
                                    OpenConnLettiWrite()
                                    dbConnConfLettiWrite.Execute(StrSql)
                                End If

                            End If

                        Else
                            ' trovata
                            'TxtLog.Text = TxtLog.Text & RsBaanVarianti("t_citg").Value.ToString & " - TROVATA NEL SITO" & vbCrLf

                            ' SELECT top 10  * from ttdftd028400 PER TROVARE IL PREZZO DEL RIVESTIMENTO SUPPLEMENTARE SI DEVE ARRIVARE PER CAT MOD E MISURA


                            StrSql = " SELECT * FROM  dbo.SCL_VAL_Scegli_Varianti"
                            StrSql = StrSql & "  WHERE VAL_Codice = '" & RsBaanVarianti("t_citg").Value.ToString & "' AND SCL_Id = " & RsLetti("SCL_Id").Value.ToString
                            OpenConnLettiWrite()
                            RsTemp = dbConnConfLettiWrite.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                TxtLog.Text = TxtLog.Text & RsBaanVarianti("t_citg").Value.ToString & " - " & RsLetti("SCL_Id").Value.ToString & " - NON TROVATO ABBINAMENTO VAL_CODICE SCL_ID NEL SITO" & vbCrLf
                                If IntCorreggiErrori = -1 Then
                                    TxtLog.Text = TxtLog.Text & RsBaanVarianti("t_citg").Value.ToString & " - " & RsLetti("SCL_Id").Value.ToString & " - AGGIUNTO TROVATO ABBINAMENTO VAL_CODICE SCL_ID NEL SITO" & vbCrLf
                                    StrSql = " INSERT INTO SCL_VAL_Scegli_Varianti ( VAL_Codice, SCL_Id )"
                                    StrSql = StrSql & "  VALUES  ( '" & RsBaanVarianti("t_citg").Value.ToString & "'," & RsLetti("SCL_Id").Value.ToString & ") "
                                    OpenConnLettiWrite()
                                    dbConnConfLettiWrite.Execute(StrSql)

                                End If

                            End If

                        End If

                        Application.DoEvents()
                        RsBaanVarianti.MoveNext()
                    End While

                End If
                TxtLog.Text = TxtLog.Text & vbCrLf & vbCrLf
                RsLetti.MoveNext()
            End While
            dbConnConfLettiRead.Close()
        Next

        On Error Resume Next
        dbConnConfLettiWrite.Close()
        dbConnBaan.Close()
        On Error GoTo 0
        TxtLog.Text = TxtLog.Text & vbCrLf & vbCrLf & "PROCEDURA FINITA"
    End Sub

    Private Sub BtnCopiaIngombri_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCopiaIngombri.Click
        ' per alcune opzioni copio gli ingombri (VMI_Varianti_Misure) delle categorie modello inserite in VAL_VARIANTI che vengono visualizzate solo a fronte della scelta dell'opzione.

        Dim RsLetti, RsMisure, RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim IntCorreggiErrori As Integer

        IntCorreggiErrori = ChkCorreggi.Checked
        TxtLog.Text = ""
        StrSql = " SELECT DISTINCT VAL_Codice, VAL_Codice_New FROM dbo.OPZLCM_OpzioniLettiFiltriCatMod GROUP BY VAL_Codice, VAL_Codice_New HAVING (NOT (VAL_Codice_New IS NULL))"
        OpenConnLettiRead()

        ' RsLetti.ActiveConnection = dbConnConfLettiRead       
        ' RsLetti.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        ' RsLetti.CursorType = ADODB.CursorTypeEnum.adOpenDynamic
        'RsLetti.LockType = ADODB.LockTypeEnum.adLockBatchOptimistic
        'RsLetti.Open(StrSql)

        RsLetti = dbConnConfLettiRead.Execute(StrSql)

        While Not RsLetti.EOF
                     
            StrSql = " SELECT * FROM dbo.VMI_Varianti_Misure WHERE (VAL_Codice = '" & RsLetti("VAL_Codice_New").Value.ToString & "') AND (VMI_IngombroMisura IS NULL)"
            RsTemp = dbConnConfLettiRead.Execute(StrSql)
            If RsTemp.BOF = True And RsTemp.EOF = True Then
                'TxtLog.Text = TxtLog.Text & " NESSUNA MISURA SENZA INGOMBRO " & vbCrLf
            Else
                While Not RsTemp.EOF


                    TxtLog.Text = TxtLog.Text & RsLetti("VAL_Codice_New").Value.ToString & " - " & RsTemp("VMI_CodiceMisuraBaan").Value.ToString & " MISURA SENZA INGOMBRO " & vbCrLf

                    StrSql = " SELECT * FROM dbo.VMI_Varianti_Misure WHERE (VAL_Codice = '" & RsLetti("VAL_Codice").Value.ToString & "') AND (VMI_CodiceMisuraBaan = '" & RsTemp("VMI_CodiceMisuraBaan").Value.ToString & "')"
                    OpenConnLettiWrite()                 
                    RsMisure = dbConnConfLettiWrite.Execute(StrSql)
                    If RsMisure.BOF = True And RsMisure.EOF = True Then
                        TxtLog.Text = TxtLog.Text & StrSql & " ---  MISURA NON TROVATA " & vbCrLf
                    Else

                        If IntCorreggiErrori = -1 Then


                            StrSql = "UPDATE VMI_Varianti_Misure SET VMI_IngombroMisura='" & RsMisure("VMI_IngombroMisura").Value.ToString & "'  WHERE (VAL_Codice = '" & RsLetti("VAL_Codice_New").Value.ToString & "') AND (VMI_CodiceMisuraBaan = '" & RsTemp("VMI_CodiceMisuraBaan").Value.ToString & "')"
                            OpenConnLettiWrite()
                            dbConnConfLettiWrite.Execute(StrSql)
                            TxtLog.Text = TxtLog.Text & RsLetti("VAL_Codice_New").Value.ToString & " - " & RsTemp("VMI_CodiceMisuraBaan").Value.ToString & " ---  MISURA CORRETTA " & vbCrLf
                        End If
                    End If

                    Application.DoEvents()
                    RsTemp.MoveNext()
                End While
            End If
            Application.DoEvents()
            RsLetti.MoveNext()
        End While

        dbConnConfLettiRead.Close()
        'dbConnConfLettiWrite.Close()

        TxtLog.Text = TxtLog.Text & vbCrLf & vbCrLf & "PROCEDURA FINITA"

    End Sub

    Private Sub CopiaImgombri()
        ' per alcune opzioni copio gli ingombri (VMI_Varianti_Misure) delle categorie modello inserite in VAL_VARIANTI che vengono visualizzate solo a fronte della scelta dell'opzione.

        Dim RsLetti, RsMisure, RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim StrVarianteFrom, StrVarianteMaster As String

        Dim IntCorreggiErrori As Integer

        IntCorreggiErrori = ChkCorreggi.Checked
        TxtLog.Text = ""

        StrVarianteFrom = "LMEN"
        StrVarianteMaster = "LMEA"
        StrSql = "SELECT * FROM dbo.VMI_Varianti_Misure WHERE (VMI_IngombroMisura IS NULL) AND (VAL_Codice = '" & StrVarianteFrom & "')"

        OpenConnLettiRead()

        ' RsLetti.ActiveConnection = dbConnConfLettiRead       
        ' RsLetti.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        ' RsLetti.CursorType = ADODB.CursorTypeEnum.adOpenDynamic
        'RsLetti.LockType = ADODB.LockTypeEnum.adLockBatchOptimistic
        'RsLetti.Open(StrSql)

        RsLetti = dbConnConfLettiRead.Execute(StrSql)

        While Not RsLetti.EOF
            TxtLog.Text = TxtLog.Text & StrVarianteFrom & " - " & RsLetti("VMI_CodiceMisura").Value.ToString & " MISURA SENZA INGOMBRO " & vbCrLf

            StrSql = " SELECT * FROM dbo.VMI_Varianti_Misure WHERE (VAL_Codice = '" & StrVarianteMaster & "') AND (VMI_CodiceMisura= '" & RsLetti("VMI_CodiceMisura").Value.ToString & "')"
            RsTemp = dbConnConfLettiRead.Execute(StrSql)
            If RsTemp.BOF = True And RsTemp.EOF = True Then
                TxtLog.Text = TxtLog.Text & " MISURA MANCANTE " & StrSql & vbCrLf & vbCrLf
            Else
                While Not RsTemp.EOF
                    TxtLog.Text = TxtLog.Text & StrVarianteFrom & " - " & RsTemp("VMI_CodiceMisura").Value.ToString & " - " & RsTemp("VMI_IngombroMisura").Value.ToString & " ---  DA CORREGGERE " & vbCrLf
                    If IntCorreggiErrori = -1 Then
                        StrSql = "UPDATE VMI_Varianti_Misure SET VMI_IngombroMisura='" & RsTemp("VMI_IngombroMisura").Value.ToString & "' WHERE (VAL_Codice = '" & StrVarianteFrom & "') AND (VMI_CodiceMisura = '" & RsLetti("VMI_CodiceMisura").Value.ToString & "')"
                        OpenConnLettiWrite()
                        dbConnConfLettiWrite.Execute(StrSql)
                        TxtLog.Text = TxtLog.Text & StrVarianteFrom & " - " & RsTemp("VMI_CodiceMisura").Value.ToString & " ---  MISURA CORRETTA " & vbCrLf
                    End If

                    Application.DoEvents()
                    RsTemp.MoveNext()
                End While
            End If
            Application.DoEvents()
            RsLetti.MoveNext()
        End While

        dbConnConfLettiRead.Close()
        'dbConnConfLettiWrite.Close()

        TxtLog.Text = TxtLog.Text & vbCrLf & vbCrLf & "PROCEDURA FINITA"

    End Sub
    Private Sub CopiaImgombriXbase()
        ' per alcune opzioni copio gli ingombri (VMI_Varianti_Misure) delle categorie modello inserite in VAL_VARIANTI che vengono visualizzate solo a fronte della scelta dell'opzione.

        Dim RsLetti, RsMisure, RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim StrVarianteFrom, StrVarianteMaster As String

        Dim IntCorreggiErrori As Integer

        IntCorreggiErrori = ChkCorreggi.Checked
        TxtLog.Text = ""

        StrSql = "SELECT * FROM VMI_Varianti_Misure INNER JOIN VAL_Varianti_Letti ON VMI_Varianti_Misure.VAL_Codice = VAL_Varianti_Letti.VAL_Codice INNER JOIN  SCL_VAL_Scegli_Varianti ON VMI_Varianti_Misure.VAL_Codice = SCL_VAL_Scegli_Varianti.VAL_Codice WHERE (VMI_Varianti_Misure.VMI_IngombroMisura IS NULL) AND (VAL_Varianti_Letti.VAL_Online > 0)"

        OpenConnLettiRead()
        RsLetti = dbConnConfLettiRead.Execute(StrSql)

        While Not RsLetti.EOF
            TxtLog.Text = TxtLog.Text & RsLetti("VAL_Codice").Value.ToString & " - " & RsLetti("VMI_CodiceMisura").Value.ToString & " MISURA SENZA INGOMBRO " & vbCrLf
            Select Case RsLetti("VMI_CodiceMisura").Value.ToString
                Case 532, 772 ' queen king 
                    TxtLog.Text = TxtLog.Text & RsLetti("VAL_Codice").Value.ToString & " - " & RsLetti("VMI_CodiceMisura").Value.ToString & " MISURA USA SENZA INGOMBRO " & vbCrLf

                Case Else
                    Select Case RsLetti("BAS_PDR_id").Value.ToString
                        Case 46 ' ingombro base h 25 = BAS_PDR_id= 19 / ALICUDI 16 
                            If RsLetti("SCL_Id").Value.ToString = 13 Or RsLetti("SCL_Id").Value.ToString = 82 Or RsLetti("SCL_Id").Value.ToString = 81 Or RsLetti("SCL_Id").Value.ToString = 77 Or RsLetti("SCL_Id").Value.ToString = 95 Or RsLetti("SCL_Id").Value.ToString = 96 Then ' ALICUDI OLIVIER
                                StrSql = " SELECT * FROM VMI_Varianti_Misure INNER JOIN VAL_Varianti_Letti ON VMI_Varianti_Misure.VAL_Codice = VAL_Varianti_Letti.VAL_Codice INNER JOIN  SCL_VAL_Scegli_Varianti ON VMI_Varianti_Misure.VAL_Codice = SCL_VAL_Scegli_Varianti.VAL_Codice  WHERE (BAS_PDR_id = 16)  and VMI_CodiceMisura =" & RsLetti("VMI_CodiceMisura").Value.ToString & "  and SCL_Id=" & RsLetti("SCL_Id").Value.ToString & " "
                            Else
                                StrSql = " SELECT * FROM VMI_Varianti_Misure INNER JOIN VAL_Varianti_Letti ON VMI_Varianti_Misure.VAL_Codice = VAL_Varianti_Letti.VAL_Codice INNER JOIN  SCL_VAL_Scegli_Varianti ON VMI_Varianti_Misure.VAL_Codice = SCL_VAL_Scegli_Varianti.VAL_Codice  WHERE (BAS_PDR_id = 19)  and VMI_CodiceMisura =" & RsLetti("VMI_CodiceMisura").Value.ToString & "  and SCL_Id=" & RsLetti("SCL_Id").Value.ToString & " "
                            End If

                            RsTemp = dbConnConfLettiRead.Execute(StrSql)


                        Case 47 ' BASE LEONARDO
                            StrSql = " SELECT * FROM VMI_Varianti_Misure INNER JOIN VAL_Varianti_Letti ON VMI_Varianti_Misure.VAL_Codice = VAL_Varianti_Letti.VAL_Codice INNER JOIN  SCL_VAL_Scegli_Varianti ON VMI_Varianti_Misure.VAL_Codice = SCL_VAL_Scegli_Varianti.VAL_Codice  WHERE (BAS_PDR_id = 5)  and VMI_CodiceMisura =" & RsLetti("VMI_CodiceMisura").Value.ToString & "  and SCL_Id=" & RsLetti("SCL_Id").Value.ToString & " "
                            RsTemp = dbConnConfLettiRead.Execute(StrSql)
                        Case Else

                            StrSql = " SELECT * FROM VMI_Varianti_Misure WHERE 1=0"
                            RsTemp = dbConnConfLettiRead.Execute(StrSql)

                    End Select

                    If RsTemp.BOF = True And RsTemp.EOF = True Then
                        TxtLog.Text = TxtLog.Text & " MISURA MANCANTE " & StrSql & vbCrLf & vbCrLf
                    Else
                        While Not RsTemp.EOF
                            If RsTemp("VMI_IngombroMisura").Value.ToString <> "" Then

                                TxtLog.Text = TxtLog.Text & RsLetti("VAL_Codice").Value.ToString & " - " & RsTemp("VMI_CodiceMisura").Value.ToString & " - " & RsTemp("VMI_IngombroMisura").Value.ToString & " ---  DA CORREGGERE " & vbCrLf
                                If IntCorreggiErrori = -1 Then

                                    StrSql = "UPDATE VMI_Varianti_Misure SET VMI_IngombroMisura='" & RsTemp("VMI_IngombroMisura").Value.ToString & "' WHERE (VAL_Codice = '" & RsLetti("VAL_Codice").Value.ToString & "') AND (VMI_CodiceMisura = '" & RsLetti("VMI_CodiceMisura").Value.ToString & "')"
                                    OpenConnLettiWrite()
                                    dbConnConfLettiWrite.Execute(StrSql)
                                    TxtLog.Text = TxtLog.Text & RsLetti("VAL_Codice").Value.ToString & " - " & RsTemp("VMI_CodiceMisura").Value.ToString & " ---  MISURA CORRETTA " & vbCrLf
                                End If
                            Else
                                TxtLog.Text = TxtLog.Text & RsLetti("VAL_Codice").Value.ToString & " - " & RsTemp("VMI_CodiceMisura").Value.ToString & " - " & RsTemp("VMI_IngombroMisura").Value.ToString & " ---  InGOMBRO NON TROVATO " & vbCrLf

                            End If
                            Application.DoEvents()
                            RsTemp.MoveNext()
                        End While
                    End If


            End Select


            Application.DoEvents()
            RsLetti.MoveNext()
        End While

        dbConnConfLettiRead.Close()
        'dbConnConfLettiWrite.Close()

        TxtLog.Text = TxtLog.Text & vbCrLf & vbCrLf & "PROCEDURA FINITA"

    End Sub
    Private Sub BtnCorreggiIngombri_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCorreggiIngombri.Click
        'CopiaImgombri()
        CopiaImgombriXbase()
    End Sub

    Private Sub NormalizzaMisure()
        Dim RsLetti, RsMisure, RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim ArrMisuraMaterasso, ArrIngombro
        Dim IntCorreggiErrori As Integer
        Dim ArrSCL_Letto As Array

        IntCorreggiErrori = ChkCorreggi.Checked
        TxtLog.Text = ""

        For Each SCL_Item In ChkListElencoLetti.CheckedItems
            ArrSCL_Letto = Split(SCL_Item, "_")

            StrSql = "SELECT * From dbo.VMI_Varianti_Misure INNER Join dbo.VAL_Varianti_Letti ON dbo.VMI_Varianti_Misure.VAL_Codice = dbo.VAL_Varianti_Letti.VAL_Codice INNER Join dbo.SCL_VAL_Scegli_Varianti ON dbo.VAL_Varianti_Letti.VAL_Codice = dbo.SCL_VAL_Scegli_Varianti.VAL_Codice INNER Join  dbo.SCL_ScegliIlTuoLetto ON dbo.SCL_VAL_Scegli_Varianti.SCL_Id = dbo.SCL_ScegliIlTuoLetto.SCL_Id Where (dbo.SCL_ScegliIlTuoLetto.SCL_Id = " & ArrSCL_Letto(0).ToString & ") And (dbo.VAL_Varianti_Letti.VAL_Online > 0) "

            OpenConnLettiRead()
            RsLetti = dbConnConfLettiRead.Execute(StrSql)

            While Not RsLetti.EOF
                TxtLog.Text = TxtLog.Text & " - " & RsLetti("VMI_DescrizioneMisura").Value.ToString & "   " & vbCrLf
                If RsLetti("VMI_IngombroMisura").Value.ToString = "" Then
                    TxtLog.Text = TxtLog.Text & "MVMI_IngombroMisura VUOTA"
                Else
                    If RsLetti("VMI_DescrizioneMisura").Value.ToString = "" Then
                        TxtLog.Text = TxtLog.Text & "VMI_DescrizioneMisura VUOTA"
                    Else
                        ArrMisuraMaterasso = Split(UCase(RsLetti("VMI_DescrizioneMisura").Value.ToString), "X")
                        ArrIngombro = Split(UCase(RsLetti("VMI_IngombroMisura").Value.ToString), "X")
                        TxtLog.Text = TxtLog.Text & ArrMisuraMaterasso(0).ToString & " - " & ArrMisuraMaterasso(1).ToString & "  - " & ArrIngombro(0).ToString & " - " & ArrIngombro(1).ToString & vbCrLf
                        StrSql = "UPDATE dbo.VMI_Varianti_Misure SET VMI_Larghezza='" & Trim(ArrMisuraMaterasso(0).ToString) & "', VMI_Lunghezza='" & Trim(ArrMisuraMaterasso(1).ToString) & "', VMI_IngombroLarghezza=" & Trim(ArrIngombro(0).ToString) & " , VMI_IngombroLunghezza=" & Trim(ArrIngombro(1).ToString) & ", VMI_IngombroAltezza=" & Trim(ArrIngombro(1).ToString) & " WHERE VMI_ID = " & RsLetti("VMI_ID").Value.ToString
                        TxtLog.Text = TxtLog.Text & StrSql & vbCrLf
                        If IntCorreggiErrori = -1 Then
                            OpenConnLettiWrite()
                            dbConnConfLettiWrite.Execute(StrSql)
                            TxtLog.Text = TxtLog.Text & "CORRETTO" & vbCrLf
                        End If
                    End If
                End If
                Application.DoEvents()
                RsLetti.MoveNext()
            End While
        Next
        dbConnConfLettiRead.Close()

        TxtLog.Text = TxtLog.Text & vbCrLf & vbCrLf & "PROCEDURA FINITA"
    End Sub

    Private Sub cmdNormalizzaMisure_Click(sender As Object, e As EventArgs) Handles cmdNormalizzaMisure.Click
        NormalizzaMisure()
    End Sub

End Class