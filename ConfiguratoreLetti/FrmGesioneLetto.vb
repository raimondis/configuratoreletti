﻿Imports System.Net


Public Class FrmGestioneLetto
    Dim ArrFrameRender(20) As String
    Dim ArrFrameRenderSingoli(20) As String
    Dim objFtp As vbnetFtpLibrary.ftp = New vbnetFtpLibrary.ftp()

    Private Sub FrmGestioneLetto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim RsLetti As New ADODB.Recordset
        Dim StrSql As String

        OpenConnLettiRead()

        StrSql = "SELECT * FROM SCL_ScegliIlTuoLetto WHERE SCL_Online > 0 or SCL_OnlineBA > 0 ORDER BY SCL_Nome"
        RsLetti = dbConnConfLettiRead.Execute(StrSql)
        While Not RsLetti.EOF
            ChkListElencoLetti.Items.Add(RsLetti("SCL_Id").Value.ToString & "_" & RsLetti("SCL_Nome").Value.ToString & " " & RsLetti("SCL_SottoNome_it").Value.ToString, False)
            RsLetti.MoveNext()
        End While

        StrSql = "SELECT LSTP_id, LSTP_CodBaan, LSTP_DesElenco_it FROM dbo.LSTP_ListiniAlPubblico WHERE LSTP_Stato <> - 1"
        RsLetti = dbConnBa.Execute(StrSql)
        While Not RsLetti.EOF
            ChkListListini.Items.Add(RsLetti("LSTP_CodBaan").Value.ToString & "_" & RsLetti("LSTP_DesElenco_it").Value.ToString, False)
            RsLetti.MoveNext()
        End While

        objFtp.UseSSL = 0

        objFtp.UserName = "businessarea.flou.it|flouftp"
        objFtp.Password = "d1v4n0zz0!"
        objFtp.Host = "217.29.160.42"


    End Sub

    Private Sub cmdControlla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdControlla.Click
        Dim RsFileDelete As New ADODB.Recordset
        Dim StrSql As String
        Dim StrFileName As String
        Dim ArrLSTP_CodBaan As Array
        Dim ArrSCL_Letto As Array

        txtResults.Text = ""

        For Each SCL_Item In ChkListElencoLetti.SelectedItems
            ArrSCL_Letto = Split(SCL_Item, "_")
            StrSql = "SELECT DISTINCT SYNCD_FileOrigine, SYNCD_FileDestinazione FROM dbo.SYNCD_SincDettaglio WHERE (SYNCD_CodContenuto IN (SELECT DISTINCT SYNCD_CodContenuto_xml FROM dbo.SYNCV_SincVersioneApp WHERE (SYNCV_app = 'let'))) AND (SYNCD_FileDestinazione LIKE '%letto_c_" & ArrSCL_Letto(0).ToString & "%')"
            RsFileDelete = dbConnBa.Execute(StrSql)
            While Not RsFileDelete.EOF

                StrFileName = Replace(RsFileDelete("SYNCD_FileDestinazione").Value.ToString, "xml/", "")
                ' cancello il file senza estensione di listino 

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrFileName)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrFileName.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    If ex.Status = WebExceptionStatus.Timeout Then
                        '''' code
                    End If
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrFileName.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try


                Try
                    objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrFileName)
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrFileName.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrFileName.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                For Each LSTP_CodBaan In ChkListListini.SelectedItems 'SCORRO I LISTINI SELEZIONATI
                    ArrLSTP_CodBaan = Split(LSTP_CodBaan, "_")

                    'scrorro i listini e cancello i file 
                    StrFileName = Replace(RsFileDelete("SYNCD_FileDestinazione").Value.ToString, "xml/", "")
                    StrFileName = Replace(StrFileName, ".xml", "_" & ArrLSTP_CodBaan(0).ToString & ".xml")

                    Try
                        objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrFileName)
                        txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrFileName.ToString & " | FATTO" & vbCrLf & vbCrLf
                    Catch ex As WebException
                        txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrFileName.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                    End Try

                    Try
                        objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrFileName)
                        txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrFileName.ToString & " | FATTO" & vbCrLf & vbCrLf
                    Catch ex As WebException
                        txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrFileName.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                    End Try
                Next


                RsFileDelete.MoveNext()
            End While

        Next


    End Sub

    Private Sub cmdRicreaFileListino_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRicreaFileListino.Click
        RicreaFileLettoApp()
    End Sub

    Private Sub RicreaFileLettoApp()
        Dim RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim StrLingue, StrLingua, StrPahtHttp, StrPathUrlXml, StrEleID, URL, StrTmp As String
        Dim StrTemp As String
        Dim ArrLingue As Array
        Dim ArrLSTP_CodBaan, ArrListini As Array
        Dim ArrSCL_Letto As Array

        Dim IntInserisciTabellaSinc, SYNCD_CodContenuto, IntForzaXMLDinamici As Integer

        Dim StrBiancheria_grt, StrRiv1_Tipo, StrRiv1_grt, StrRiv2_Tipo, StrRiv2_grt As String
        Dim IntChiamPerOgniFinituraR1, IntChiamPerOgniFinituraR2 As Integer

        'varibili per utilizzo xml
        Dim load_ok As Integer
        Dim xml_letto, xml_Finitura1, xml_Finitura2 As New MSXML2.DOMDocument60
        Dim rootletto, rootFinitura1, rootFinitura2 As MSXML2.IXMLDOMElement

        'IntSalvaXML = 1
        IntForzaXMLDinamici = 1  ' non forzare  0 forza

        Dim objFSO, objTextFile
        Dim strDirectory, strFile
        'strDirectory = server.MapPath("xml")
        ' Create the File System Object
        objFSO = CreateObject("Scripting.FileSystemObject")
        ' OpenTextFile Method needs a Const value
        ' ForAppending = 8 ForReading = 1, ForWriting = 2
        Const ForWriting = 2

        StrLingue = "it,en,fr,de,es" ' IMPOSTO LE LINGUE SU CUI FARE IL RESET SE NON è UNA MODIFICA DU UN TESTO TRADOTTO VANNO IMPOSTATE TUTTE LE LINGUE
        'StrLingue = "IT"       

        StrPahtHttp = "https://businessarea.flou.it/cl/asp/genxml/"  ' PERCORSO CHE CHIAMA LA PAGINA PER RICREARE L'XML DA USARE NELLE APP 

        Dim StrListini, StrListino As String
        StrListini = ""
        For Each LSTP_CodBaan In ChkListListini.CheckedItems 'SCORRO I LISTINI SELEZIONATI
            ArrLSTP_CodBaan = Split(LSTP_CodBaan, "_")

            If StrListini = "" Then
                StrListini = StrListini & ArrLSTP_CodBaan(0).ToString
            Else
                StrListini = StrListini & "," & ArrLSTP_CodBaan(0).ToString
            End If
        Next

        ArrLingue = Split(StrLingue, ",")
        ArrListini = Split(StrListini, ",")

        IntInserisciTabellaSinc = 1 ' UTILIZZATA PER GENERARE RECORD DI AGGIORNAMENTO DATI PER LE APP

        Dim dbConnLocal As ADODB.Connection
        dbConnLocal = New ADODB.Connection
        StrPathUrlXml = ""
        If IntInserisciTabellaSinc = 1 Then
            SYNCD_CodContenuto = 2001 ' DEFINIRE IN QUESTA PROCEDURA VALORE CAMBIA AGGIORNAMENTO SYNC GENERALE
            StrPathUrlXml = "https://businessarea.flou.it/cl/apps/xml/"
            dbConnLocal.Open("Provider=SQLOLEDB;Data Source=appdb.dom_flou.it\FLOUDB; Initial Catalog=Flou_ba_intra; User Id=sa;Password=notturno")
        End If

        xml_letto.async = False

        Dim HttpXML As Object
        HttpXML = CreateObject("Microsoft.XMLHTTP")

        Dim SecondsOut, sResolve, sConnect, sSend, sReceive  ' Variabili per XMLHTTP
        SecondsOut = 25  ' How many seconds to wait for responsefrom XMLA
        sResolve = SecondsOut * 1000 : sConnect = SecondsOut * 1000 : sSend = SecondsOut * 1000 : sReceive = SecondsOut * 100000
        'HttpXML.SetTimeouts  (sResolve, sConnect, sSend, sReceive)
        ' HttpXML.SetTimeouts(sResolve, sConnect, sSend, sReceive)

        StrTemp = ""
        For intLingua = 0 To UBound(ArrLingue)       ' SCORRO LE LINGUE SELEZIONATE
            StrLingua = ArrLingue(intLingua)
            txtResults.Text = txtResults.Text & vbCrLf & " INIZIO LINGUA " & StrLingua & vbCrLf

            For Each SCL_Item In ChkListElencoLetti.CheckedItems    ' SCORRO I LETTI SELEZIONATI
                ArrSCL_Letto = Split(SCL_Item, "_")
                StrEleID = ArrSCL_Letto(0).ToString

                ' CHIAMO LA PAGINA DI SETUP LETTO  http://www.flou.it/cl/letto.asp?eleid=48
                StrTemp = "letto_" & StrEleID & "_" & StrLingua & ".xml"

                If IntInserisciTabellaSinc = 1 Then
                    StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml.ToString & StrTemp.ToString & "'"
                    RsTemp = dbConnLocal.Execute(StrSql)
                    If RsTemp.BOF = True And RsTemp.EOF = True Then
                        StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                        StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                        StrSql = StrSql & "'GET','XML' ) "
                        dbConnLocal.Execute(StrSql)
                    End If
                End If

                On Error Resume Next
                objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
                txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO"
                If Err.Number <> 0 Then
                    txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                Else
                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                End If
                On Error GoTo 0

                On Error Resume Next
                objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp) ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA BUSINESS AREA
                txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO"
                If Err.Number <> 0 Then
                    txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                Else
                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                End If
                On Error GoTo 0

                On Error Resume Next
                objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)   ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER IL WWWW
                txtResults.Text = txtResults.Text & "DELETE FILE | /www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO"
                If Err.Number <> 0 Then
                    txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                Else
                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                End If
                On Error GoTo 0

                ' RICREO IL FILE NELLA CARTELLA DI APPOGGIO DELLA APP IL CONTENUTO INOLTRE MI SERVE PER DECIDERE COME CHIAMARE LA LETTO_C.ASP E GENERARE I FILE PER LE FINITURE
                URL = StrPahtHttp & "letto.asp?eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                txtResults.Text = txtResults.Text & vbCrLf & URL & vbCrLf
                HttpXML.open("GET", URL, False)
                HttpXML.send()

                load_ok = xml_letto.loadXML(HttpXML.responseText)
                If load_ok = False Then
                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & vbCrLf & "ERRORE - letto.asp"
                    Exit Sub
                End If
                rootletto = xml_letto.documentElement

                '------------------------ DISABILITATO SPOSTATO NELLA PROCEDURA DI RESET BIANCHERIE -----------------------------------------
                'BIANCHERIA
                If 1 = 0 Then ' DISABILITO IL CALCOLO DEI FILE BIANCHERIA
                    ' ESTRAGGO IL GRUPPO ASSOCIATO
                    On Error Resume Next
                    StrBiancheria_grt = rootletto.selectSingleNode("biancheria").attributes(2).nodeTypedValue 'rootletto.selectSingleNode("biancheria").attributes("grt").nodeTypedValue
                    On Error GoTo 0

                    '--
                    If StrBiancheria_grt = "" Then
                        'VECCHIO METODO DI CALCOLO GLI XML SONO PERSONALI AL LETTO --> PER OGNI LETTO 3 XML  PER APP PIU VECCHIE PUBBLICATE PRIMA DEL 2013   
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_" & StrEleID & "_VAR_bia_ti_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_ti&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        HttpXML.open("GET", URL, False)
                        HttpXML.send()

                        '--
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_" & StrEleID & "_VAR_bia_no_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_no&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        HttpXML.open("GET", URL, False)
                        HttpXML.send()

                        '--
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_" & StrEleID & "_VAR_bia_co_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_co&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        HttpXML.open("GET", URL, False)
                        HttpXML.send()
                    Else
                        ' NUOVO METODO SONO RAGGRUPPATI PER GRT --> 2 XML PER TUTTI I LETTI
                        ' LA PROCEDURA FA COMUNQUE UNA CHIAMATA PER OGNI LETTO MA PA PAGINA ASP NON RIGENERA L'XML SE LO A GIA CHIAMATO UNA VOLTA

                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_VAR_bia_ti_" & StrBiancheria_grt & "_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_ti&grt=" & StrBiancheria_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        HttpXML.open("GET", URL, False)
                        HttpXML.send()

                        '--
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_VAR_bia_no_" & StrBiancheria_grt & "_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_no&grt=" & StrBiancheria_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        HttpXML.open("GET", URL, False)
                        HttpXML.send()

                        '--
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_VAR_bia_co_" & StrBiancheria_grt & "_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_co&grt=" & StrBiancheria_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        HttpXML.open("GET", URL, False)
                        HttpXML.send()


                    End If
                End If
                ' FINE BIANCHERIE
                '------------------------ DISABILITATO SPOSTATO NELLA PROCEDURA DI RESET BIANCHERIE -----------------------------------------


                'ANALISI RIVESTIMENTI - NECESSARIA PER INDIVIDUARE I PARAMETRI DI CHIAMATA DELLA LETTO_C.ASP -- FINITURE MATERIALI

                ' DECIDERE  CHIAMARE IN FUNZIONE DEL LETTO
                On Error Resume Next
                StrRiv1_Tipo = rootletto.selectSingleNode("rivestimento1").attributes(0).nodeTypedValue 'rootletto.selectSingleNode("rivestimento1").attributes("tipo").nodeTypedValue
                StrRiv1_grt = rootletto.selectSingleNode("rivestimento1").attributes(2).nodeTypedValue 'rootletto.selectSingleNode("rivestimento1").attributes("grt").nodeTypedValue
                On Error GoTo 0
                IntChiamPerOgniFinituraR1 = 0
                Select Case StrRiv1_Tipo
                    Case "0"
                        ' TESSILE
                        '--
                        If StrRiv1_grt = "" Then ' QUESTO LETTO NON RIENTRA IN NESSUN GRUPPO XML VECCHIO STILE

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_riv_no_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_no&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_TI_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_riv_CP_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_CP&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        Else
                            ' NUOVO METODO SONO RAGGRUPPATI PER GRT 
                            ' LA PROCEDURA FA COMUNQUE UNA CHIAMATA PER OGNI LETTO MA LA PAGINA ASP NON RIGENERA L'XML SE LO A GIA CHIAMATO UNA VOLTA

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_riv_no_" & StrRiv1_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_no&grt=" & StrRiv1_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_TI_" & StrRiv1_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&grt=" & StrRiv1_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_riv_CP_" & StrRiv1_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_CP&grt=" & StrRiv1_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        End If

                    Case "1"
                        ' LE FINITURE AL 2012 NON SONO MAI RAGGRUPPATE PER GRT_ID
                        ' finiture VALUTARE PER PASSAGGIO PARAMETRO
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_FIN_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_FIN&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        HttpXML.open("GET", URL, False)
                        HttpXML.send()

                        On Error Resume Next
                        StrTmp = rootletto.selectSingleNode("rivestimento1").attributes(1).nodeTypedValue 'rootletto.selectSingleNode("rivestimento1").attributes("par").nodeTypedValue
                        On Error GoTo 0

                        If StrTmp = "1" Then
                            ' response.Write "IntChiamPerOgniFinituraR1 =" &  IntChiamPerOgniFinituraR1
                            ' response.End
                            IntChiamPerOgniFinituraR1 = 1
                            load_ok = xml_Finitura1.loadXML(HttpXML.responseText)
                            If load_ok = False Then
                                'Response.Write("ERRORE - pgateway.asp - finitura 1<br>" & URL & "<br>" & HttpXML.responseText)
                                'Response.End()
                                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & vbCrLf & "ERRORE - pgateway.asp - finitura 1" & vbCrLf & URL & vbCrLf & HttpXML.responseText
                                Exit Sub
                            End If
                            rootFinitura1 = xml_Finitura1.documentElement
                        End If

                    Case ""

                End Select

                On Error Resume Next
                StrRiv2_Tipo = rootletto.selectSingleNode("rivestimento2").attributes(0).nodeTypedValue ' rootletto.selectSingleNode("rivestimento2").attributes("tipo").nodeTypedValue
                StrRiv2_grt = rootletto.selectSingleNode("rivestimento2").attributes(2).nodeTypedValue ' rootletto.selectSingleNode("rivestimento2").attributes("grt").nodeTypedValue
                On Error GoTo 0
                IntChiamPerOgniFinituraR2 = 0
                Select Case StrRiv2_Tipo
                    Case "0"
                        ' TESSILE
                        If StrRiv2_grt = "" Then ' QUESTO LETTO NON RIENTRA IN NESSUN GRUPPO XML VECCHIO STILE
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_NO_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_NO&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_TI_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_CP_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_CP&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        Else
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_NO_" & StrRiv2_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_NO&grt=" & StrRiv2_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_TI_" & StrRiv2_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&grt=" & StrRiv2_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_CP_" & StrRiv2_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_CP&grt=" & StrRiv2_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        End If
                    Case "1"
                        ' LE FINITURE AL 2012 NON SONO MAI RAGGRUPPATE PER GRT_ID
                        ' finiture VALUTARE PER PASSAGGIO PARAMETRO
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_FIN_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_FIN&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        'response.Write(URL & "<br>")
                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf

                        HttpXML.open("GET", URL, False)
                        HttpXML.send()
                        On Error Resume Next
                        StrTmp = rootletto.selectSingleNode("rivestimento2").attributes(1).nodeTypedValue 'rootletto.selectSingleNode("rivestimento2").attributes("par").nodeTypedValue
                        On Error GoTo 0
                        If StrTmp = "1" Then
                            IntChiamPerOgniFinituraR2 = 1
                            load_ok = xml_Finitura1.loadXML(HttpXML.responseText)
                            If load_ok = False Then
                                ' Response.Write("ERRORE - pgateway.asp - finitura 2")
                                'Response.End()
                                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & vbCrLf & "ERRORE - pgateway.asp - finitura 2" & vbCrLf & URL & vbCrLf & HttpXML.responseText
                                Exit Sub
                            End If
                            rootFinitura2 = xml_Finitura1.documentElement
                        End If

                    Case ""

                End Select

                'GESTIONE CHIAMATA PAGINA LETTO_C.ASP
                'QUESTA CHIAMATA VA FATTA PER OGNI LISTINO INCLUDE I PREZZI DEL MATERASSI ETC.

                If StrListini = "" Then
                    'SENZA PREZZI
                    'http://baflou/cl/letto_c.asp?bia=0000%5F0000&riv1=0031%5FN013&eleid=14&riv2=
                    If IntChiamPerOgniFinituraR1 = 0 And IntChiamPerOgniFinituraR2 = 0 Then
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "letto_c_" & StrEleID & "_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If

                        On Error Resume Next
                        objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
                        txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO"
                        If Err.Number <> 0 Then
                            txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                        Else
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                        End If
                        On Error GoTo 0

                        On Error Resume Next
                        objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp) ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA BUSINESS AREA
                        txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO"
                        If Err.Number <> 0 Then
                            txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                        Else
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                        End If
                        On Error GoTo 0

                        On Error Resume Next
                        objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)   ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER IL WWWW
                        txtResults.Text = txtResults.Text & "DELETE FILE | /www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO"
                        If Err.Number <> 0 Then
                            txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                        Else
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                        End If
                        On Error GoTo 0


                        URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=0000%5F0000&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=NON&StrLangGX=" & StrLingua
                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf

                        HttpXML.open("GET", URL, False)
                        HttpXML.send()
                    Else
                        'GESTIONE NOME E  SALVATAGGIO FILE 
                        If IntChiamPerOgniFinituraR1 = 1 Then
                            For Each NodeBlocco In rootFinitura1.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                ' response.Write NodeBlocco.selectSingleNode("id").Text
                                'response.Write "<br>" & NodeBlocco.selectSingleNode("foglia/id").Text
                                'response.Write NodeBlocco.selectSingleNode("id").Text
                                For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                    If IntInserisciTabellaSinc = 1 Then
                                        StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & ".xml"
                                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                        RsTemp = dbConnLocal.Execute(StrSql)
                                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                            StrSql = StrSql & "'GET','XML' ) "
                                            dbConnLocal.Execute(StrSql)
                                        End If
                                    End If

                                    On Error Resume Next
                                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
                                    txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO"
                                    If Err.Number <> 0 Then
                                        txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                    Else
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                    End If
                                    On Error GoTo 0

                                    On Error Resume Next
                                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp) ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA BUSINESS AREA
                                    txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO"
                                    If Err.Number <> 0 Then
                                        txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                    Else
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                    End If
                                    On Error GoTo 0

                                    On Error Resume Next
                                    objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)   ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER IL WWWW
                                    txtResults.Text = txtResults.Text & "DELETE FILE | /www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO"
                                    If Err.Number <> 0 Then
                                        txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                    Else
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                    End If
                                    On Error GoTo 0

                                    URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=NON&StrLangGX=" & StrLingua
                                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                    HttpXML.open("GET", URL, False)
                                    HttpXML.send()

                                Next
                            Next
                        End If
                        If IntChiamPerOgniFinituraR2 = 1 Then
                            For Each NodeBlocco In rootFinitura2.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                'StrNomeRiv2= "&riv2=0000%5F0000"
                                For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                    If IntInserisciTabellaSinc = 1 Then
                                        StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & ".xml"
                                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                        RsTemp = dbConnLocal.Execute(StrSql)
                                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                            StrSql = StrSql & "'GET','XML' ) "
                                            dbConnLocal.Execute(StrSql)
                                        End If
                                    End If


                                    On Error Resume Next
                                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
                                    txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO"
                                    If Err.Number <> 0 Then
                                        txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                    Else
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                    End If
                                    On Error GoTo 0

                                    On Error Resume Next
                                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp) ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA BUSINESS AREA
                                    txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO"
                                    If Err.Number <> 0 Then
                                        txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                    Else
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                    End If
                                    On Error GoTo 0

                                    On Error Resume Next
                                    objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)   ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER IL WWWW
                                    txtResults.Text = txtResults.Text & "DELETE FILE | /www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO"
                                    If Err.Number <> 0 Then
                                        txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                    Else
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                    End If
                                    On Error GoTo 0

                                    URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv2=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv1=0000%5F0000&Stat=" & IntForzaXMLDinamici & "&StrListGX=NON&StrLangGX=" & StrLingua
                                    'response.Write(URL & "<br>")
                                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                    HttpXML.open("GET", URL, False)
                                    HttpXML.send()
                                Next
                            Next
                        End If

                    End If
                Else
                    'CON PREZZI DA ESEGUIRE PER OGNI LISTINO
                    For intListino = 0 To UBound(ArrListini)            'SCORRO I LISTINI
                        StrListino = ArrListini(intListino)
                        'http://baflou/cl/letto_c.asp?bia=0000%5F0000&riv1=0031%5FN013&eleid=14&riv2=
                        If IntChiamPerOgniFinituraR1 = 0 And IntChiamPerOgniFinituraR2 = 0 Then
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "letto_c_" & StrEleID & "_" & StrLingua & "_#UTENTE_LISTINO_RIV_PRIMARIO.xml" ' non metto il listino nel nome da inserire nella lista xml altrimenti avrei righe inutili è la procedura asp che decide il listino
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If

                            StrTemp = Replace(StrTemp, "#UTENTE_LISTINO_RIV_PRIMARIO", StrListino)
                            On Error Resume Next
                            objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
                            txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO"
                            If Err.Number <> 0 Then
                                txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                            Else
                                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                            End If
                            On Error GoTo 0

                            On Error Resume Next
                            objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp) ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA BUSINESS AREA
                            txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO"
                            If Err.Number <> 0 Then
                                txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                            Else
                                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                            End If
                            On Error GoTo 0

                            On Error Resume Next
                            objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)   ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER IL WWWW
                            txtResults.Text = txtResults.Text & "DELETE FILE | /www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO"
                            If Err.Number <> 0 Then
                                txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                            Else
                                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                            End If
                            On Error GoTo 0

                            URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=0000%5F0000&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        Else
                            'GESTIONE NOME E  SALVATAGGIO FILE 
                            If IntChiamPerOgniFinituraR1 = 1 Then
                                For Each NodeBlocco In rootFinitura1.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                    ' response.Write NodeBlocco.selectSingleNode("id").Text
                                    'response.Write "<br>" & NodeBlocco.selectSingleNode("foglia/id").Text
                                    'response.Write NodeBlocco.selectSingleNode("id").Text
                                    For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                        If IntInserisciTabellaSinc = 1 Then
                                            StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & "_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                            RsTemp = dbConnLocal.Execute(StrSql)
                                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                                StrSql = StrSql & "'GET','XML' ) "
                                                dbConnLocal.Execute(StrSql)
                                            End If
                                        End If

                                        StrTemp = Replace(StrTemp, "#UTENTE_LISTINO_RIV_PRIMARIO", StrListino)
                                        On Error Resume Next
                                        objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
                                        txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO"
                                        If Err.Number <> 0 Then
                                            txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                        Else
                                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                        End If
                                        On Error GoTo 0

                                        On Error Resume Next
                                        objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp) ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA BUSINESS AREA
                                        txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO"
                                        If Err.Number <> 0 Then
                                            txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                        Else
                                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                        End If
                                        On Error GoTo 0

                                        On Error Resume Next
                                        objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)   ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER IL WWWW
                                        txtResults.Text = txtResults.Text & "DELETE FILE | /www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO"
                                        If Err.Number <> 0 Then
                                            txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                        Else
                                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                        End If
                                        On Error GoTo 0

                                        URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                                        ' response.Write(URL & "<br>")
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                        HttpXML.open("GET", URL, False)
                                        HttpXML.send()

                                    Next
                                Next
                            End If
                            If IntChiamPerOgniFinituraR2 = 1 Then
                                For Each NodeBlocco In rootFinitura2.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                    'StrNomeRiv2= "&riv2=0000%5F0000"
                                    For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                        If IntInserisciTabellaSinc = 1 Then
                                            StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & "_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                            RsTemp = dbConnLocal.Execute(StrSql)
                                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                                StrSql = StrSql & "'GET','XML' ) "
                                                dbConnLocal.Execute(StrSql)
                                            End If
                                        End If

                                        StrTemp = Replace(StrTemp, "#UTENTE_LISTINO_RIV_PRIMARIO", StrListino)
                                        On Error Resume Next
                                        objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
                                        txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO"
                                        If Err.Number <> 0 Then
                                            txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                        Else
                                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                        End If
                                        On Error GoTo 0

                                        On Error Resume Next
                                        objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp) ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA BUSINESS AREA
                                        txtResults.Text = txtResults.Text & "DELETE FILE | /businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO"
                                        If Err.Number <> 0 Then
                                            txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                        Else
                                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                        End If
                                        On Error GoTo 0

                                        On Error Resume Next
                                        objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)   ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER IL WWWW
                                        txtResults.Text = txtResults.Text & "DELETE FILE | /www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO"
                                        If Err.Number <> 0 Then
                                            txtResults.Text = txtResults.Text & "errore - " & Err.Description & vbCrLf & vbCrLf
                                        Else
                                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                                        End If
                                        On Error GoTo 0

                                        URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv2=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv1=0000%5F0000&Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                                        'response.Write(URL & "<br>")
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                        HttpXML.open("GET", URL, False)
                                        HttpXML.send()
                                    Next
                                Next
                            End If

                        End If


                        If 1 = 0 Then
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pletti_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            'GENERO IL FILE PLIST PER LETTI E COPRIPIUMINI
                            URL = StrPahtHttp & "pletti.asp?Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                            'response.Write(URL & "<br>")
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "psico_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "psico.asp?Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                            'response.Write(URL & "<br>")
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        End If
                    Next

                End If

            Next
        Next

        'ObjReference = Nothing

        'response.Write("FINITO")
        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & "FINITO" & vbCrLf


    End Sub


    Private Sub RicreaFileLettoAppTOTALE()
        Dim RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim StrLingue, StrLingua, StrPahtHttp, StrPathUrlXml, StrEleID, URL, StrTmp As String
        Dim StrTemp As String
        Dim ArrLingue As Array
        Dim ArrLSTP_CodBaan, ArrListini As Array
        Dim ArrSCL_Letto As Array

        Dim IntInserisciTabellaSinc, SYNCD_CodContenuto, IntForzaXMLDinamici As Integer

        Dim StrBiancheria_grt, StrRiv1_Tipo, StrRiv1_grt, StrRiv2_Tipo, StrRiv2_grt As String
        Dim IntChiamPerOgniFinituraR1, IntChiamPerOgniFinituraR2 As Integer

        'varibili per utilizzo xml
        Dim load_ok As Integer
        Dim xml_letto, xml_Finitura1, xml_Finitura2 As New MSXML2.DOMDocument60
        Dim rootletto, rootFinitura1, rootFinitura2 As MSXML2.IXMLDOMElement

        'IntSalvaXML = 1
        IntForzaXMLDinamici = 1  ' non forzare  0 forza

        Dim objFSO, objTextFile
        Dim strDirectory, strFile
        'strDirectory = server.MapPath("xml")
        ' Create the File System Object
        objFSO = CreateObject("Scripting.FileSystemObject")
        ' OpenTextFile Method needs a Const value
        ' ForAppending = 8 ForReading = 1, ForWriting = 2
        Const ForWriting = 2

        StrLingue = "it,en,fr,de,es"

        'StrLingue = "IT"
        'StrListini = "538,PUX,PUS,PUA,PUC"
        'StrListini = "653,PUX,PUC,PUS,PUA,PUG"
        ' StrListini = "PES"

        'StrPahtHttp = "http://www.flou.it/cl/"
        'StrPahtHttp = "http://pc156/cl/"
        StrPahtHttp = "https://businessarea.flou.it/cl/asp/genxml/"

        Dim StrListini, StrListino As String
        StrListini = ""
        For Each LSTP_CodBaan In ChkListListini.SelectedItems 'SCORRO I LISTINI SELEZIONATI
            ArrLSTP_CodBaan = Split(LSTP_CodBaan, "_")

            If StrListini = "" Then
                StrListini = StrListini & ArrLSTP_CodBaan(0).ToString
            Else
                StrListini = StrListini & "," & ArrLSTP_CodBaan(0).ToString
            End If

        Next


        ArrLingue = Split(StrLingue, ",")
        ArrListini = Split(StrListini, ",")

        IntInserisciTabellaSinc = 1 ' UTILIZZATA PER GENERARE RECORD DI AGGIORNAMENTO DATI PER LE APP

        Dim dbConnLocal As ADODB.Connection
        dbConnLocal = New ADODB.Connection
        StrPathUrlXml = ""
        If IntInserisciTabellaSinc = 1 Then
            SYNCD_CodContenuto = 2001 ' DEFINIRE IN QUESTA PROCEDURA VALORE CAMBIA AGGIORNAMENTO SYNC GENERALE
            'StrPathUrlXml = "http://www.flou.it/cl/apps/xml/"
            StrPathUrlXml = "https://businessarea.flou.it/cl/apps/xml/"

            dbConnLocal.Open("Provider=SQLOLEDB;Data Source=FLOU2; Initial Catalog=Flou_ba_intra; User Id=sa;Password=")

        End If


        'xml_letti = CreateObject("Msxml2.DomDocument")
        'xml_letto = CreateObject("Msxml2.DomDocument")
        ' xml_Finitura1 = CreateObject("Msxml2.DomDocument")
        ' xml_Finitura2 = CreateObject("Msxml2.DomDocument")
        'xml_letti.async = False
        xml_letto.async = False

        Dim HttpXML As Object
        HttpXML = CreateObject("Microsoft.XMLHTTP")

        Dim SecondsOut, sResolve, sConnect, sSend, sReceive  ' Variabili per XMLHTTP
        SecondsOut = 25  ' How many seconds to wait for responsefrom XMLA
        sResolve = SecondsOut * 1000 : sConnect = SecondsOut * 1000 : sSend = SecondsOut * 1000 : sReceive = SecondsOut * 100000
        'HttpXML.SetTimeouts  (sResolve, sConnect, sSend, sReceive)
        ' HttpXML.SetTimeouts(sResolve, sConnect, sSend, sReceive)
        StrTemp = ""
        For intLingua = 0 To UBound(ArrLingue)
            StrLingua = ArrLingue(intLingua)
            Debug.Print(" INIZIO LINGUA " & StrLingua & "<br>")

            For Each SCL_Item In ChkListElencoLetti.SelectedItems
                ArrSCL_Letto = Split(SCL_Item, "_")
                StrEleID = ArrSCL_Letto(0).ToString
                ' chiama  http://www.flou.it/cl/letto.asp?eleid=48
                StrTemp = "letto_" & StrEleID & "_" & StrLingua & ".xml"

                If IntInserisciTabellaSinc = 1 Then
                    StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml.ToString & StrTemp.ToString & "'"
                    RsTemp = dbConnLocal.Execute(StrSql)
                    If RsTemp.BOF = True And RsTemp.EOF = True Then
                        StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                        StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                        StrSql = StrSql & "'GET','XML' ) "
                        dbConnLocal.Execute(StrSql)
                    End If
                End If

                URL = StrPahtHttp & "letto.asp?eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                'response.Write(URL & "<br>")
                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                HttpXML.open("GET", URL, False)
                HttpXML.send()

                load_ok = xml_letto.loadXML(HttpXML.responseText)
                If load_ok = False Then
                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & vbCrLf & "ERRORE - letto.asp"
                    Exit Sub
                End If
                rootletto = xml_letto.documentElement

                'BIANCHERIA
                ' ESTRAGGO IL GRUPPO ASSOCIATO
                On Error Resume Next
                StrBiancheria_grt = rootletto.selectSingleNode("biancheria").attributes(2).nodeTypedValue 'rootletto.selectSingleNode("biancheria").attributes("grt").nodeTypedValue
                On Error GoTo 0

                '--
                If StrBiancheria_grt = "" Then
                    'VECCHIO METODO DI CALCOLO GLI XML SONO PERSONALI AL LETTO --> PER OGNI LETTO 3 XML  PER APP PIU VECCHIE PUBBLICATE PRIMA DEL 2013   
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_" & StrEleID & "_VAR_bia_ti_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_ti&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()

                    '--
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_" & StrEleID & "_VAR_bia_no_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_no&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()

                    '--
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_" & StrEleID & "_VAR_bia_co_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_co&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()
                Else
                    ' NUOVO METODO SONO RAGGRUPPATI PER GRT --> 2 XML PER TUTTI I LETTI
                    ' LA PROCEDURA FA COMUNQUE UNA CHIAMATA PER OGNI LETTO MA PA PAGINA ASP NON RIGENERA L'XML SE LO A GIA CHIAMATO UNA VOLTA

                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_VAR_bia_ti_" & StrBiancheria_grt & "_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_ti&grt=" & StrBiancheria_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()

                    '--
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_VAR_bia_no_" & StrBiancheria_grt & "_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_no&grt=" & StrBiancheria_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()

                    '--
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_VAR_bia_co_" & StrBiancheria_grt & "_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_co&grt=" & StrBiancheria_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()

                End If
                ' FINE BIANCHERIA

                'RIVESTIMENTI
                ' DECIDERE  CHIAMARE IN FUNZIONE DEL LETTO
                On Error Resume Next
                StrRiv1_Tipo = rootletto.selectSingleNode("rivestimento1").attributes(0).nodeTypedValue 'rootletto.selectSingleNode("rivestimento1").attributes("tipo").nodeTypedValue
                StrRiv1_grt = rootletto.selectSingleNode("rivestimento1").attributes(2).nodeTypedValue 'rootletto.selectSingleNode("rivestimento1").attributes("grt").nodeTypedValue
                On Error GoTo 0
                IntChiamPerOgniFinituraR1 = 0
                Select Case StrRiv1_Tipo
                    Case "0"
                        ' TESSILE
                        '--
                        If StrRiv1_grt = "" Then ' QUESTO LETTO NON RIENTRA IN NESSUN GRUPPO XML VECCHIO STILE

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_riv_no_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_no&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_TI_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_riv_CP_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_CP&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        Else
                            ' NUOVO METODO SONO RAGGRUPPATI PER GRT 
                            ' LA PROCEDURA FA COMUNQUE UNA CHIAMATA PER OGNI LETTO MA LA PAGINA ASP NON RIGENERA L'XML SE LO A GIA CHIAMATO UNA VOLTA

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_riv_no_" & StrRiv1_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_no&grt=" & StrRiv1_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_TI_" & StrRiv1_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&grt=" & StrRiv1_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_riv_CP_" & StrRiv1_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_CP&grt=" & StrRiv1_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        End If

                    Case "1"
                        ' LE FINITURE AL 2012 NON SONO MAI RAGGRUPPATE PER GRT_ID
                        ' finiture VALUTARE PER PASSAGGIO PARAMETRO
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_FIN_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_FIN&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        HttpXML.open("GET", URL, False)
                        HttpXML.send()

                        On Error Resume Next
                        StrTmp = rootletto.selectSingleNode("rivestimento1").attributes(1).nodeTypedValue 'rootletto.selectSingleNode("rivestimento1").attributes("par").nodeTypedValue
                        On Error GoTo 0

                        If StrTmp = "1" Then
                            ' response.Write "IntChiamPerOgniFinituraR1 =" &  IntChiamPerOgniFinituraR1
                            ' response.End
                            IntChiamPerOgniFinituraR1 = 1
                            load_ok = xml_Finitura1.loadXML(HttpXML.responseText)
                            If load_ok = False Then
                                'Response.Write("ERRORE - pgateway.asp - finitura 1<br>" & URL & "<br>" & HttpXML.responseText)
                                'Response.End()
                                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & vbCrLf & "ERRORE - pgateway.asp - finitura 1" & vbCrLf & URL & vbCrLf & HttpXML.responseText
                                Exit Sub
                            End If
                            rootFinitura1 = xml_Finitura1.documentElement
                        End If

                    Case ""

                End Select

                On Error Resume Next
                StrRiv2_Tipo = rootletto.selectSingleNode("rivestimento2").attributes(0).nodeTypedValue ' rootletto.selectSingleNode("rivestimento2").attributes("tipo").nodeTypedValue
                StrRiv2_grt = rootletto.selectSingleNode("rivestimento2").attributes(2).nodeTypedValue ' rootletto.selectSingleNode("rivestimento2").attributes("grt").nodeTypedValue
                On Error GoTo 0
                IntChiamPerOgniFinituraR2 = 0
                Select Case StrRiv2_Tipo
                    Case "0"
                        ' TESSILE
                        If StrRiv2_grt = "" Then ' QUESTO LETTO NON RIENTRA IN NESSUN GRUPPO XML VECCHIO STILE
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_NO_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_NO&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_TI_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_CP_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_CP&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        Else
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_NO_" & StrRiv2_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_NO&grt=" & StrRiv2_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_TI_" & StrRiv2_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&grt=" & StrRiv2_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_CP_" & StrRiv2_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_CP&grt=" & StrRiv2_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        End If
                    Case "1"
                        ' LE FINITURE AL 2012 NON SONO MAI RAGGRUPPATE PER GRT_ID
                        ' finiture VALUTARE PER PASSAGGIO PARAMETRO
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_FIN_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_FIN&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        'response.Write(URL & "<br>")
                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf

                        HttpXML.open("GET", URL, False)
                        HttpXML.send()
                        On Error Resume Next
                        StrTmp = rootletto.selectSingleNode("rivestimento2").attributes(1).nodeTypedValue 'rootletto.selectSingleNode("rivestimento2").attributes("par").nodeTypedValue
                        On Error GoTo 0
                        If StrTmp = "1" Then
                            IntChiamPerOgniFinituraR2 = 1
                            load_ok = xml_Finitura1.loadXML(HttpXML.responseText)
                            If load_ok = False Then
                                ' Response.Write("ERRORE - pgateway.asp - finitura 2")
                                'Response.End()
                                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & vbCrLf & "ERRORE - pgateway.asp - finitura 2" & vbCrLf & URL & vbCrLf & HttpXML.responseText
                                Exit Sub
                            End If
                            rootFinitura2 = xml_Finitura1.documentElement
                        End If

                    Case ""

                End Select

                'GESTIONE CHIAMATA PAGINA 
                'QUESTA CHIAMATA VA FATTA PER OGNI LISTINO INCLUDE I PREZZI DEL MATERASSI ETC.

                If StrListini = "" Then
                    'SENZA PREZZI
                    'http://baflou/cl/letto_c.asp?bia=0000%5F0000&riv1=0031%5FN013&eleid=14&riv2=
                    If IntChiamPerOgniFinituraR1 = 0 And IntChiamPerOgniFinituraR2 = 0 Then
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "letto_c_" & StrEleID & "_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=0000%5F0000&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=NON&StrLangGX=" & StrLingua
                        ' response.Write(URL & "<br>")
                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf

                        HttpXML.open("GET", URL, False)
                        HttpXML.send()
                    Else
                        'GESTIONE NOME E  SALVATAGGIO FILE 
                        If IntChiamPerOgniFinituraR1 = 1 Then
                            For Each NodeBlocco In rootFinitura1.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                ' response.Write NodeBlocco.selectSingleNode("id").Text
                                'response.Write "<br>" & NodeBlocco.selectSingleNode("foglia/id").Text
                                'response.Write NodeBlocco.selectSingleNode("id").Text
                                For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                    If IntInserisciTabellaSinc = 1 Then
                                        StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & ".xml"
                                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                        RsTemp = dbConnLocal.Execute(StrSql)
                                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                            StrSql = StrSql & "'GET','XML' ) "
                                            dbConnLocal.Execute(StrSql)
                                        End If
                                    End If

                                    URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=NON&StrLangGX=" & StrLingua
                                    'response.Write(URL & "<br>")
                                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                    HttpXML.open("GET", URL, False)
                                    HttpXML.send()

                                Next
                            Next
                        End If
                        If IntChiamPerOgniFinituraR2 = 1 Then
                            For Each NodeBlocco In rootFinitura2.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                'StrNomeRiv2= "&riv2=0000%5F0000"
                                For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                    If IntInserisciTabellaSinc = 1 Then
                                        StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & ".xml"
                                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                        RsTemp = dbConnLocal.Execute(StrSql)
                                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                            StrSql = StrSql & "'GET','XML' ) "
                                            dbConnLocal.Execute(StrSql)
                                        End If
                                    End If

                                    URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv2=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv1=0000%5F0000&Stat=" & IntForzaXMLDinamici & "&StrListGX=NON&StrLangGX=" & StrLingua
                                    'response.Write(URL & "<br>")
                                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                    HttpXML.open("GET", URL, False)
                                    HttpXML.send()
                                Next
                            Next
                        End If

                    End If
                Else
                    'CON PREZZI DA ESEGUIRE PER OGNI LISTINO
                    For intListino = 0 To UBound(ArrListini)
                        StrListino = ArrListini(intListino)
                        'http://baflou/cl/letto_c.asp?bia=0000%5F0000&riv1=0031%5FN013&eleid=14&riv2=
                        If IntChiamPerOgniFinituraR1 = 0 And IntChiamPerOgniFinituraR2 = 0 Then
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "letto_c_" & StrEleID & "_" & StrLingua & "_#UTENTE_LISTINO_RIV_PRIMARIO.xml" ' non metto il listino nel nome da inserire nella lista xml altrimenti avrei righe inutili è la procedura asp che decide il listino
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=0000%5F0000&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                            'response.Write(URL & "<br>")
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        Else
                            'GESTIONE NOME E  SALVATAGGIO FILE 
                            If IntChiamPerOgniFinituraR1 = 1 Then
                                For Each NodeBlocco In rootFinitura1.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                    ' response.Write NodeBlocco.selectSingleNode("id").Text
                                    'response.Write "<br>" & NodeBlocco.selectSingleNode("foglia/id").Text
                                    'response.Write NodeBlocco.selectSingleNode("id").Text
                                    For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                        If IntInserisciTabellaSinc = 1 Then
                                            StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & "_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                            RsTemp = dbConnLocal.Execute(StrSql)
                                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                                StrSql = StrSql & "'GET','XML' ) "
                                                dbConnLocal.Execute(StrSql)
                                            End If
                                        End If

                                        URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                                        ' response.Write(URL & "<br>")
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                        HttpXML.open("GET", URL, False)
                                        HttpXML.send()

                                    Next
                                Next
                            End If
                            If IntChiamPerOgniFinituraR2 = 1 Then
                                For Each NodeBlocco In rootFinitura2.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                    'StrNomeRiv2= "&riv2=0000%5F0000"
                                    For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                        If IntInserisciTabellaSinc = 1 Then
                                            StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & "_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                            RsTemp = dbConnLocal.Execute(StrSql)
                                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                                StrSql = StrSql & "'GET','XML' ) "
                                                dbConnLocal.Execute(StrSql)
                                            End If
                                        End If

                                        URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv2=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv1=0000%5F0000&Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                                        'response.Write(URL & "<br>")
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                        HttpXML.open("GET", URL, False)
                                        HttpXML.send()
                                    Next
                                Next
                            End If

                        End If


                        If 1 = 0 Then
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pletti_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            'GENERO IL FILE PLIST PER LETTI E COPRIPIUMINI
                            URL = StrPahtHttp & "pletti.asp?Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                            'response.Write(URL & "<br>")
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "psico_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "psico.asp?Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                            'response.Write(URL & "<br>")
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        End If
                    Next

                End If

            Next
        Next

        'ObjReference = Nothing

        'response.Write("FINITO")
        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & "FINITO" & vbCrLf


    End Sub

    Private Sub RicreaFileBiaRiv()
        Dim RsTemp As New ADODB.Recordset
        Dim StrSql As String
        Dim StrLingue, StrLingua, StrPahtHttp, StrPathUrlXml, StrEleID, URL, StrTmp As String
        Dim StrTemp As String
        Dim ArrLingue As Array
        Dim ArrLSTP_CodBaan, ArrListini As Array
        Dim ArrSCL_Letto As Array

        Dim IntInserisciTabellaSinc, SYNCD_CodContenuto, IntForzaXMLDinamici As Integer

        Dim StrBiancheria_grt, StrRiv1_Tipo, StrRiv1_grt, StrRiv2_Tipo, StrRiv2_grt As String
        Dim IntChiamPerOgniFinituraR1, IntChiamPerOgniFinituraR2 As Integer

        'varibili per utilizzo xml
        Dim load_ok As Integer
        Dim xml_letto, xml_Finitura1, xml_Finitura2 As New MSXML2.DOMDocument60
        Dim rootletto, rootFinitura1, rootFinitura2 As MSXML2.IXMLDOMElement

        'IntSalvaXML = 1
        IntForzaXMLDinamici = 1  ' non forzare  0 forza

        Dim objFSO, objTextFile
        Dim strDirectory, strFile
        'strDirectory = server.MapPath("xml")
        ' Create the File System Object
        objFSO = CreateObject("Scripting.FileSystemObject")
        ' OpenTextFile Method needs a Const value
        ' ForAppending = 8 ForReading = 1, ForWriting = 2
        Const ForWriting = 2

        StrLingue = "it,en,fr,de,es"

        'StrLingue = "IT"
        'StrListini = "538,PUX,PUS,PUA,PUC"
        'StrListini = "653,PUX,PUC,PUS,PUA,PUG"
        ' StrListini = "PES"

        'StrPahtHttp = "http://www.flou.it/cl/"
        'StrPahtHttp = "http://pc156/cl/"
        StrPahtHttp = "https://businessarea.flou.it/cl/asp/genxml/"

        Dim StrListini, StrListino As String
        StrListini = ""
        For Each LSTP_CodBaan In ChkListListini.SelectedItems 'SCORRO I LISTINI SELEZIONATI
            ArrLSTP_CodBaan = Split(LSTP_CodBaan, "_")

            If StrListini = "" Then
                StrListini = StrListini & ArrLSTP_CodBaan(0).ToString
            Else
                StrListini = StrListini & "," & ArrLSTP_CodBaan(0).ToString
            End If

        Next


        ArrLingue = Split(StrLingue, ",")
        ArrListini = Split(StrListini, ",")

        IntInserisciTabellaSinc = 1 ' UTILIZZATA PER GENERARE RECORD DI AGGIORNAMENTO DATI PER LE APP

        Dim dbConnLocal As ADODB.Connection
        dbConnLocal = New ADODB.Connection
        StrPathUrlXml = ""
        If IntInserisciTabellaSinc = 1 Then
            SYNCD_CodContenuto = 2001 ' DEFINIRE IN QUESTA PROCEDURA VALORE CAMBIA AGGIORNAMENTO SYNC GENERALE
            'StrPathUrlXml = "http://www.flou.it/cl/apps/xml/"
            StrPathUrlXml = "https://businessarea.flou.it/cl/apps/xml/"

            dbConnLocal.Open("Provider=SQLOLEDB;Data Source=appdb.dom_flou.it\FLOUDB; Initial Catalog=Flou_ba_intra; User Id=sa;Password=notturno")

        End If


        'xml_letti = CreateObject("Msxml2.DomDocument")
        'xml_letto = CreateObject("Msxml2.DomDocument")
        ' xml_Finitura1 = CreateObject("Msxml2.DomDocument")
        ' xml_Finitura2 = CreateObject("Msxml2.DomDocument")
        'xml_letti.async = False
        xml_letto.async = False

        Dim HttpXML As Object
        HttpXML = CreateObject("Microsoft.XMLHTTP")

        Dim SecondsOut, sResolve, sConnect, sSend, sReceive  ' Variabili per XMLHTTP
        SecondsOut = 25  ' How many seconds to wait for responsefrom XMLA
        sResolve = SecondsOut * 1000 : sConnect = SecondsOut * 1000 : sSend = SecondsOut * 1000 : sReceive = SecondsOut * 100000
        'HttpXML.SetTimeouts  (sResolve, sConnect, sSend, sReceive)
        ' HttpXML.SetTimeouts(sResolve, sConnect, sSend, sReceive)
        StrTemp = ""
        For intLingua = 0 To UBound(ArrLingue)
            StrLingua = ArrLingue(intLingua)
            Debug.Print(" INIZIO LINGUA " & StrLingua & "<br>")

            For Each SCL_Item In ChkListElencoLetti.SelectedItems
                ArrSCL_Letto = Split(SCL_Item, "_")
                StrEleID = ArrSCL_Letto(0).ToString
                ' chiama  http://www.flou.it/cl/letto.asp?eleid=48

                If IntInserisciTabellaSinc = 1 Then
                    StrTemp = "letto_" & StrEleID & "_" & StrLingua & ".xml"
                    StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml.ToString & StrTemp.ToString & "'"
                    RsTemp = dbConnLocal.Execute(StrSql)
                    If RsTemp.BOF = True And RsTemp.EOF = True Then
                        StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                        StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                        StrSql = StrSql & "'GET','XML' ) "
                        dbConnLocal.Execute(StrSql)
                    End If
                End If

                objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)

                URL = StrPahtHttp & "letto.asp?eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                'response.Write(URL & "<br>")
                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                HttpXML.open("GET", URL, False)
                HttpXML.send()

                load_ok = xml_letto.loadXML(HttpXML.responseText)
                If load_ok = False Then
                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & vbCrLf & "ERRORE - letto.asp"
                    Exit Sub
                End If
                rootletto = xml_letto.documentElement

                'BIANCHERIA
                ' ESTRAGGO IL GRUPPO ASSOCIATO
                On Error Resume Next
                StrBiancheria_grt = rootletto.selectSingleNode("biancheria").attributes(2).nodeTypedValue 'rootletto.selectSingleNode("biancheria").attributes("grt").nodeTypedValue
                On Error GoTo 0

                '--
                If StrBiancheria_grt = "" Then
                    'VECCHIO METODO DI CALCOLO GLI XML SONO PERSONALI AL LETTO --> PER OGNI LETTO 3 XML  PER APP PIU VECCHIE PUBBLICATE PRIMA DEL 2013   
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_" & StrEleID & "_VAR_bia_ti_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_ti&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()

                    '--
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_" & StrEleID & "_VAR_bia_no_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_no&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()

                    '--
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_" & StrEleID & "_VAR_bia_co_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_co&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()
                Else
                    ' NUOVO METODO SONO RAGGRUPPATI PER GRT --> 2 XML PER TUTTI I LETTI
                    ' LA PROCEDURA FA COMUNQUE UNA CHIAMATA PER OGNI LETTO MA PA PAGINA ASP NON RIGENERA L'XML SE LO A GIA CHIAMATO UNA VOLTA

                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_VAR_bia_ti_" & StrBiancheria_grt & "_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_ti&grt=" & StrBiancheria_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()

                    '--
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_VAR_bia_no_" & StrBiancheria_grt & "_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_no&grt=" & StrBiancheria_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()

                    '--
                    If IntInserisciTabellaSinc = 1 Then
                        StrTemp = "pgateway_VAR_bia_co_" & StrBiancheria_grt & "_" & StrLingua & ".xml"
                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                        RsTemp = dbConnLocal.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                            StrSql = StrSql & "'GET','XML' ) "
                            dbConnLocal.Execute(StrSql)
                        End If
                    End If
                    URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_co&grt=" & StrBiancheria_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()


                End If
                'RIVESTIMENTI
                ' DECIDERE  CHIAMARE IN FUNZIONE DEL LETTO
                On Error Resume Next
                StrRiv1_Tipo = rootletto.selectSingleNode("rivestimento1").attributes(0).nodeTypedValue 'rootletto.selectSingleNode("rivestimento1").attributes("tipo").nodeTypedValue
                StrRiv1_grt = rootletto.selectSingleNode("rivestimento1").attributes(2).nodeTypedValue 'rootletto.selectSingleNode("rivestimento1").attributes("grt").nodeTypedValue
                On Error GoTo 0
                IntChiamPerOgniFinituraR1 = 0
                Select Case StrRiv1_Tipo
                    Case "0"
                        ' TESSILE
                        '--
                        If StrRiv1_grt = "" Then ' QUESTO LETTO NON RIENTRA IN NESSUN GRUPPO XML VECCHIO STILE

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_riv_no_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_no&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_TI_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_riv_CP_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_CP&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        Else
                            ' NUOVO METODO SONO RAGGRUPPATI PER GRT 
                            ' LA PROCEDURA FA COMUNQUE UNA CHIAMATA PER OGNI LETTO MA LA PAGINA ASP NON RIGENERA L'XML SE LO A GIA CHIAMATO UNA VOLTA

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_riv_no_" & StrRiv1_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_no&grt=" & StrRiv1_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_TI_" & StrRiv1_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&grt=" & StrRiv1_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            '--
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_riv_CP_" & StrRiv1_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_CP&grt=" & StrRiv1_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        End If

                    Case "1"
                        ' LE FINITURE AL 2012 NON SONO MAI RAGGRUPPATE PER GRT_ID
                        ' finiture VALUTARE PER PASSAGGIO PARAMETRO
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_FIN_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_FIN&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        HttpXML.open("GET", URL, False)
                        HttpXML.send()

                        On Error Resume Next
                        StrTmp = rootletto.selectSingleNode("rivestimento1").attributes(1).nodeTypedValue 'rootletto.selectSingleNode("rivestimento1").attributes("par").nodeTypedValue
                        On Error GoTo 0

                        If StrTmp = "1" Then
                            ' response.Write "IntChiamPerOgniFinituraR1 =" &  IntChiamPerOgniFinituraR1
                            ' response.End
                            IntChiamPerOgniFinituraR1 = 1
                            load_ok = xml_Finitura1.loadXML(HttpXML.responseText)
                            If load_ok = False Then
                                'Response.Write("ERRORE - pgateway.asp - finitura 1<br>" & URL & "<br>" & HttpXML.responseText)
                                'Response.End()
                                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & vbCrLf & "ERRORE - pgateway.asp - finitura 1" & vbCrLf & URL & vbCrLf & HttpXML.responseText
                                Exit Sub
                            End If
                            rootFinitura1 = xml_Finitura1.documentElement
                        End If

                    Case ""

                End Select

                On Error Resume Next
                StrRiv2_Tipo = rootletto.selectSingleNode("rivestimento2").attributes(0).nodeTypedValue ' rootletto.selectSingleNode("rivestimento2").attributes("tipo").nodeTypedValue
                StrRiv2_grt = rootletto.selectSingleNode("rivestimento2").attributes(2).nodeTypedValue ' rootletto.selectSingleNode("rivestimento2").attributes("grt").nodeTypedValue
                On Error GoTo 0
                IntChiamPerOgniFinituraR2 = 0
                Select Case StrRiv2_Tipo
                    Case "0"
                        ' TESSILE
                        If StrRiv2_grt = "" Then ' QUESTO LETTO NON RIENTRA IN NESSUN GRUPPO XML VECCHIO STILE
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_NO_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_NO&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_TI_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_CP_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_CP&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        Else
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_NO_" & StrRiv2_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_NO&grt=" & StrRiv2_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_TI_" & StrRiv2_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&grt=" & StrRiv2_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pgateway_VAR_RIV_CP_" & StrRiv2_grt & "_" & StrLingua & ".xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_CP&grt=" & StrRiv2_grt & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        End If
                    Case "1"
                        ' LE FINITURE AL 2012 NON SONO MAI RAGGRUPPATE PER GRT_ID
                        ' finiture VALUTARE PER PASSAGGIO PARAMETRO
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "pgateway_" & StrEleID & "_VAR_RIV_FIN_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_FIN&eleid=" & StrEleID & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                        'response.Write(URL & "<br>")
                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf

                        HttpXML.open("GET", URL, False)
                        HttpXML.send()
                        On Error Resume Next
                        StrTmp = rootletto.selectSingleNode("rivestimento2").attributes(1).nodeTypedValue 'rootletto.selectSingleNode("rivestimento2").attributes("par").nodeTypedValue
                        On Error GoTo 0
                        If StrTmp = "1" Then
                            IntChiamPerOgniFinituraR2 = 1
                            load_ok = xml_Finitura1.loadXML(HttpXML.responseText)
                            If load_ok = False Then
                                ' Response.Write("ERRORE - pgateway.asp - finitura 2")
                                'Response.End()
                                txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & vbCrLf & "ERRORE - pgateway.asp - finitura 2" & vbCrLf & URL & vbCrLf & HttpXML.responseText
                                Exit Sub
                            End If
                            rootFinitura2 = xml_Finitura1.documentElement
                        End If

                    Case ""

                End Select

                'GESTIONE CHIAMATA PAGINA 
                'QUESTA CHIAMATA VA FATTA PER OGNI LISTINO INCLUDE I PREZZI DEL MATERASSI ETC.

                If StrListini = "" Then
                    'SENZA PREZZI
                    'http://baflou/cl/letto_c.asp?bia=0000%5F0000&riv1=0031%5FN013&eleid=14&riv2=
                    If IntChiamPerOgniFinituraR1 = 0 And IntChiamPerOgniFinituraR2 = 0 Then
                        If IntInserisciTabellaSinc = 1 Then
                            StrTemp = "letto_c_" & StrEleID & "_" & StrLingua & ".xml"
                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                            RsTemp = dbConnLocal.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                StrSql = StrSql & "'GET','XML' ) "
                                dbConnLocal.Execute(StrSql)
                            End If
                        End If
                        URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=0000%5F0000&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=NON&StrLangGX=" & StrLingua
                        ' response.Write(URL & "<br>")
                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf

                        HttpXML.open("GET", URL, False)
                        HttpXML.send()
                    Else
                        'GESTIONE NOME E  SALVATAGGIO FILE 
                        If IntChiamPerOgniFinituraR1 = 1 Then
                            For Each NodeBlocco In rootFinitura1.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                ' response.Write NodeBlocco.selectSingleNode("id").Text
                                'response.Write "<br>" & NodeBlocco.selectSingleNode("foglia/id").Text
                                'response.Write NodeBlocco.selectSingleNode("id").Text
                                For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                    If IntInserisciTabellaSinc = 1 Then
                                        StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & ".xml"
                                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                        RsTemp = dbConnLocal.Execute(StrSql)
                                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                            StrSql = StrSql & "'GET','XML' ) "
                                            dbConnLocal.Execute(StrSql)
                                        End If
                                    End If

                                    URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=NON&StrLangGX=" & StrLingua
                                    'response.Write(URL & "<br>")
                                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                    HttpXML.open("GET", URL, False)
                                    HttpXML.send()

                                Next
                            Next
                        End If
                        If IntChiamPerOgniFinituraR2 = 1 Then
                            For Each NodeBlocco In rootFinitura2.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                'StrNomeRiv2= "&riv2=0000%5F0000"
                                For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                    If IntInserisciTabellaSinc = 1 Then
                                        StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & ".xml"
                                        StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                        RsTemp = dbConnLocal.Execute(StrSql)
                                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                                            StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                            StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                                            StrSql = StrSql & "'GET','XML' ) "
                                            dbConnLocal.Execute(StrSql)
                                        End If
                                    End If

                                    URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv2=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv1=0000%5F0000&Stat=" & IntForzaXMLDinamici & "&StrListGX=NON&StrLangGX=" & StrLingua
                                    'response.Write(URL & "<br>")
                                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                    HttpXML.open("GET", URL, False)
                                    HttpXML.send()
                                Next
                            Next
                        End If

                    End If
                Else
                    'CON PREZZI DA ESEGUIRE PER OGNI LISTINO
                    For intListino = 0 To UBound(ArrListini)
                        StrListino = ArrListini(intListino)
                        'http://baflou/cl/letto_c.asp?bia=0000%5F0000&riv1=0031%5FN013&eleid=14&riv2=
                        If IntChiamPerOgniFinituraR1 = 0 And IntChiamPerOgniFinituraR2 = 0 Then
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "letto_c_" & StrEleID & "_" & StrLingua & "_#UTENTE_LISTINO_RIV_PRIMARIO.xml" ' non metto il listino nel nome da inserire nella lista xml altrimenti avrei righe inutili è la procedura asp che decide il listino
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=0000%5F0000&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                            'response.Write(URL & "<br>")
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        Else
                            'GESTIONE NOME E  SALVATAGGIO FILE 
                            If IntChiamPerOgniFinituraR1 = 1 Then
                                For Each NodeBlocco In rootFinitura1.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                    ' response.Write NodeBlocco.selectSingleNode("id").Text
                                    'response.Write "<br>" & NodeBlocco.selectSingleNode("foglia/id").Text
                                    'response.Write NodeBlocco.selectSingleNode("id").Text
                                    For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                        If IntInserisciTabellaSinc = 1 Then
                                            StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & "_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                            RsTemp = dbConnLocal.Execute(StrSql)
                                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                                StrSql = StrSql & "'GET','XML' ) "
                                                dbConnLocal.Execute(StrSql)
                                            End If
                                        End If

                                        URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv1=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv2=&Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                                        ' response.Write(URL & "<br>")
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                        HttpXML.open("GET", URL, False)
                                        HttpXML.send()

                                    Next
                                Next
                            End If
                            If IntChiamPerOgniFinituraR2 = 1 Then
                                For Each NodeBlocco In rootFinitura2.selectNodes("categoria/tessuto") ' SCORRO I BLOCCHI
                                    'StrNomeRiv2= "&riv2=0000%5F0000"
                                    For Each NodeFoglia In NodeBlocco.selectNodes("foglia") ' SCORRO I BLOCCHI

                                        If IntInserisciTabellaSinc = 1 Then
                                            StrTemp = "letto_c_" & StrEleID & "_" & NodeBlocco.selectSingleNode("id").Text & "_" & NodeFoglia.selectSingleNode("id").Text & "_" & StrLingua & "_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                            StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                            RsTemp = dbConnLocal.Execute(StrSql)
                                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                                                StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                                StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                                StrSql = StrSql & "'GET','XML' ) "
                                                dbConnLocal.Execute(StrSql)
                                            End If
                                        End If

                                        URL = StrPahtHttp & "letto_c.asp?bia=0000%5F0000&riv2=" & NodeBlocco.selectSingleNode("id").Text & "%5F" & NodeFoglia.selectSingleNode("id").Text & "&eleid=" & StrEleID & "&riv1=0000%5F0000&Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                                        'response.Write(URL & "<br>")
                                        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                                        HttpXML.open("GET", URL, False)
                                        HttpXML.send()
                                    Next
                                Next
                            End If

                        End If


                        If 1 = 0 Then
                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "pletti_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            'GENERO IL FILE PLIST PER LETTI E COPRIPIUMINI
                            URL = StrPahtHttp & "pletti.asp?Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                            'response.Write(URL & "<br>")
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()

                            If IntInserisciTabellaSinc = 1 Then
                                StrTemp = "psico_#UTENTE_LISTINO_RIV_PRIMARIO.xml"
                                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                                RsTemp = dbConnLocal.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & Replace(StrTemp, "_#UTENTE_LISTINO_RIV_PRIMARIO", "") & "',"
                                    StrSql = StrSql & "'GET','XML' ) "
                                    dbConnLocal.Execute(StrSql)
                                End If
                            End If
                            URL = StrPahtHttp & "psico.asp?Stat=" & IntForzaXMLDinamici & "&StrListGX=" & StrListino & "&StrLangGX=" & StrLingua
                            'response.Write(URL & "<br>")
                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & URL & vbCrLf
                            HttpXML.open("GET", URL, False)
                            HttpXML.send()
                        End If
                    Next

                End If

            Next
        Next

        'ObjReference = Nothing

        'response.Write("FINITO")
        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & "FINITO" & vbCrLf


    End Sub

    Private Sub cmdAzzeraBiaRiv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAzzeraBiaRiv.Click

        txtResults.Text = ""

        AzzeraBia()
        AzzeraRiv()
        ScaricaFileXmlApp(820)
        ScaricaFileXmlApp(830)
        ' CaricaFileXmlApp(820)
        '  CaricaFileXmlApp(830)
    End Sub

    Private Sub AzzeraRiv()
        Dim RsTemp, RsGrpTess As New ADODB.Recordset
        Dim StrTemp, StrSql, StrLingua, StrPathUrlXml, StrPahtHttp, URL, StrLingue As String
        Dim dbConnLocal As ADODB.Connection
        Dim ArrLingue As Array
        Dim SYNCD_CodContenuto, IntForzaXMLDinamici As Integer
        Dim HttpXML As Object

        HttpXML = CreateObject("Microsoft.XMLHTTP")
        IntForzaXMLDinamici = 1  ' non forzare  0 forza
        StrLingue = "it,en,fr,de,es"
        ArrLingue = Split(StrLingue, ",")

        SYNCD_CodContenuto = 830 'DEFINIRE IN QUESTA PROCEDURA VALORE CAMBIA AGGIORNAMENTO SYNC GENERALE

        StrPathUrlXml = "https://businessarea.flou.it/cl/apps/xml/"
        StrPahtHttp = "https://businessarea.flou.it/cl/asp/genxml/" ' percorso file che generano i file xml APP configuratore letti

        dbConnLocal = New ADODB.Connection
        'dbConnLocal.Open("Provider=SQLOLEDB;Data Source=FLOU2; Initial Catalog=Flou_ba_intra; User Id=sa;Password=")
        dbConnLocal.Open(StrConnBa)

        StrSql = "DELETE FROM SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto
        dbConnLocal.Execute(StrSql)

        txtResults.Text = txtResults.Text & "RESET RIVESTIMENTI - START" & vbCrLf & vbCrLf
        StrSql = "select distinct * from (SELECT DISTINCT GRT_id_1 FROM dbo.SCL_ScegliIlTuoLetto WHERE SCL_Online = 1 UNION SELECT DISTINCT GRT_id_2 FROM dbo.SCL_ScegliIlTuoLetto AS SCL_ScegliIlTuoLetto_1 WHERE SCL_Online = 1 ) as tbl where GRT_id_1 Is Not null "

        RsGrpTess = dbConnConfLettiRead.Execute(StrSql)
        While Not RsGrpTess.EOF

            For intLingua = 0 To UBound(ArrLingue)
                StrLingua = ArrLingue(intLingua)

                StrTemp = "pgateway_VAR_riv_no_" & RsGrpTess("GRT_id_1").Value.ToString & "_" & StrLingua & ".xml"
                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                RsTemp = dbConnLocal.Execute(StrSql)
                If RsTemp.BOF = True And RsTemp.EOF = True Then
                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                    StrSql = StrSql & "'GET','XML' ) "
                    dbConnLocal.Execute(StrSql)
                End If

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    If ex.Status = WebExceptionStatus.Timeout Then
                        '''' code
                    End If
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_no&grt=" & RsGrpTess("GRT_id_1").Value.ToString & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua

                Try
                    HttpXML.open("GET", URL, False)
                    HttpXML.send()
                Catch e As WebException
                    MsgBox("aaaa" & e.Message)
                Catch e As Exception
                    MsgBox("aaaa" & e.Message)
                End Try

                '--
                StrTemp = "pgateway_VAR_RIV_TI_" & RsGrpTess("GRT_id_1").Value.ToString & "_" & StrLingua & ".xml"
                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                RsTemp = dbConnLocal.Execute(StrSql)
                If RsTemp.BOF = True And RsTemp.EOF = True Then
                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                    StrSql = StrSql & "'GET','XML' ) "
                    dbConnLocal.Execute(StrSql)
                End If

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    If ex.Status = WebExceptionStatus.Timeout Then
                        '''' code
                    End If
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                URL = StrPahtHttp & "pgateway.asp?action=VAR_RIV_TI&grt=" & RsGrpTess("GRT_id_1").Value.ToString & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                HttpXML.open("GET", URL, False)
                HttpXML.send()

                '--
                StrTemp = "pgateway_VAR_riv_CP_" & RsGrpTess("GRT_id_1").Value.ToString & "_" & StrLingua & ".xml"
                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                RsTemp = dbConnLocal.Execute(StrSql)
                If RsTemp.BOF = True And RsTemp.EOF = True Then
                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                    StrSql = StrSql & "'GET','XML' ) "
                    dbConnLocal.Execute(StrSql)
                End If

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    If ex.Status = WebExceptionStatus.Timeout Then
                        '''' code
                    End If
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                URL = StrPahtHttp & "pgateway.asp?action=VAR_riv_CP&grt=" & RsGrpTess("GRT_id_1").Value.ToString & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                HttpXML.open("GET", URL, False)
                HttpXML.send()

            Next
            RsGrpTess.MoveNext()
        End While
        RsGrpTess = Nothing

        txtResults.Text = txtResults.Text & "RESET RIVESTIMENTI - END" & vbCrLf & vbCrLf
    End Sub

    Private Sub AzzeraBia()
        Dim RsTemp As New ADODB.Recordset
        Dim StrTemp, StrSql, StrLingua, StrPathUrlXml, StrPahtHttp, URL, StrLingue As String
        Dim dbConnLocal As ADODB.Connection
        Dim ArrLingue As Array
        Dim SYNCD_CodContenuto, IntForzaXMLDinamici As Integer
        Dim HttpXML As Object

        HttpXML = CreateObject("Microsoft.XMLHTTP")
        IntForzaXMLDinamici = 1  ' non forzare  0 forza
        StrLingue = "it,en,fr,de,es"
        ArrLingue = Split(StrLingue, ",")

        SYNCD_CodContenuto = 820 ' DEFINIRE IN QUESTA PROCEDURA VALORE CAMBIA AGGIORNAMENTO SYNC GENERALE

        StrPathUrlXml = "https://businessarea.flou.it/cl/apps/xml/"
        StrPahtHttp = "https://businessarea.flou.it/cl/asp/genxml/" ' percorso file che generano i file xml APP configuratore letti

        dbConnLocal = New ADODB.Connection
        'dbConnLocal.Open("Provider=SQLOLEDB;Data Source=FLOU2; Initial Catalog=Flou_ba_intra; User Id=sa;Password=")
        dbConnLocal.Open(StrConnBa)

        StrSql = "DELETE FROM SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto
        dbConnLocal.Execute(StrSql)

        txtResults.Text = txtResults.Text & "RESET BIANCHERIA - START" & vbCrLf & vbCrLf

        'BIANCHERIA

        ' NUOVO METODO SONO RAGGRUPPATI PER GRT --> 2 XML PER TUTTI I LETTI
        ' LA PROCEDURA FA COMUNQUE UNA CHIAMATA PER OGNI LETTO MA PA PAGINA ASP NON RIGENERA L'XML SE LO A GIA CHIAMATO UNA VOLTA
        For intLingua = 0 To UBound(ArrLingue)
            StrLingua = ArrLingue(intLingua)

            For IntDimensione = 1 To 2
                '--
                StrTemp = "pgateway_VAR_bia_ti_" & IntDimensione & "_" & StrLingua & ".xml"
                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                RsTemp = dbConnLocal.Execute(StrSql)
                If RsTemp.BOF = True And RsTemp.EOF = True Then
                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                    StrSql = StrSql & "'GET','XML' ) "
                    dbConnLocal.Execute(StrSql)
                End If

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    If ex.Status = WebExceptionStatus.Timeout Then
                        '''' code
                    End If
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_ti&grt=" & IntDimensione & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                HttpXML.open("GET", URL, False)
                HttpXML.send()

                '--
                StrTemp = "pgateway_VAR_bia_no_" & IntDimensione & "_" & StrLingua & ".xml"
                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                RsTemp = dbConnLocal.Execute(StrSql)
                If RsTemp.BOF = True And RsTemp.EOF = True Then
                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                    StrSql = StrSql & "'GET','XML' ) "
                    dbConnLocal.Execute(StrSql)
                End If

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    If ex.Status = WebExceptionStatus.Timeout Then
                        '''' code
                    End If
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_no&grt=" & IntDimensione & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                HttpXML.open("GET", URL, False)
                HttpXML.send()

                '--
                StrTemp = "pgateway_VAR_bia_co_" & IntDimensione & "_" & StrLingua & ".xml"
                StrSql = "SELECT * FROM  SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto & " AND SYNCD_FileOrigine = '" & StrPathUrlXml & StrTemp & "'"
                RsTemp = dbConnLocal.Execute(StrSql)
                If RsTemp.BOF = True And RsTemp.EOF = True Then
                    StrSql = "INSERT INTO SYNCD_SincDettaglio (SYNCD_CodContenuto,SYNCD_FileOrigine,SYNCD_FileDestinazione,SYNCD_TypeOpe,SYNCD_TypeFile) Values (" & SYNCD_CodContenuto & ", "
                    StrSql = StrSql & "'" & StrPathUrlXml & StrTemp & "','xml/" & StrTemp & "',"
                    StrSql = StrSql & "'GET','XML' ) "
                    dbConnLocal.Execute(StrSql)
                End If

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/c/l/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    If ex.Status = WebExceptionStatus.Timeout Then
                        '''' code
                    End If
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/c/l/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/www.flou.it/wwwroot2012/cl/asp/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/www.flou.it/wwwroot2012/cl/asp/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                Try
                    objFtp.DeleteFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", StrTemp)
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf
                Catch ex As WebException
                    txtResults.Text = txtResults.Text & "/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                End Try

                URL = StrPahtHttp & "pgateway.asp?action=VAR_bia_co&grt=" & IntDimensione & "&Stat=" & IntForzaXMLDinamici & "&StrLangGX=" & StrLingua
                HttpXML.open("GET", URL, False)
                HttpXML.send()

            Next
        Next
        txtResults.Text = txtResults.Text & "RESET BIANCHERIA - END" & vbCrLf & vbCrLf
    End Sub

    Private Sub ScaricaFileXmlApp(ByVal SYNCD_CodContenuto As Integer)
        Dim RsTemp As New ADODB.Recordset
        Dim StrTemp, StrSql, StrPathUrlXml As String
        Dim dbConnLocal As ADODB.Connection

        StrPathUrlXml = "https://businessarea.flou.it/cl/apps/xml/"

        dbConnLocal = New ADODB.Connection
        'dbConnLocal.Open("Provider=SQLOLEDB;Data Source=FLOU2; Initial Catalog=Flou_ba_intra; User Id=sa;Password=")
        dbConnLocal.Open(StrConnBa)

        StrSql = "SELECT * FROM SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto
        RsTemp = dbConnLocal.Execute(StrSql)

        txtResults.Text = txtResults.Text & "DOWNLOAD FILE XML PER APP CODICE:" & SYNCD_CodContenuto & " - START" & vbCrLf & vbCrLf

        While Not RsTemp.EOF
            StrTemp = Replace(RsTemp("SYNCD_FileOrigine").Value.ToString, StrPathUrlXml, "")  '"simo_prova.xml"
            Try
                objFtp.DownloadFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", "d:\Websites\tmpvb\", StrTemp.ToString)
                'objFtp.DownloadFile("/businessarea.flou.it/wwwroot/cl/asp/genxml/xml/", "C:\Website\tmpvb\", "simo_prova.xml")

                txtResults.Text = txtResults.Text & "DOWNLOAD /businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf

            Catch ex As WebException
                If ex.Status = WebExceptionStatus.Timeout Then
                    '''' code
                End If
                txtResults.Text = txtResults.Text & "DOWNLOAD /businessarea.flou.it/wwwroot/cl/asp/genxml/xml/" & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
            End Try
            RsTemp.MoveNext()
        End While

        txtResults.Text = txtResults.Text & "FINE DOWNLOAD FILE XML PER APP CODICE:" & SYNCD_CodContenuto & " - END" & vbCrLf & vbCrLf

    End Sub

    Private Sub CaricaFileXmlApp(ByVal SYNCD_CodContenuto As Integer)
        Dim RsTemp As New ADODB.Recordset
        Dim StrTemp, StrSql, StrPathUrlXml, StrPathFtpUpload As String
        Dim dbConnLocal As ADODB.Connection

        StrPathUrlXml = "https://businessarea.flou.it/cl/apps/xml/"
        StrPathFtpUpload = "/businessarea.flou.it/wwwroot/cl/apps/xml_1_3_4/"

        dbConnLocal = New ADODB.Connection
        'dbConnLocal.Open("Provider=SQLOLEDB;Data Source=FLOU2; Initial Catalog=Flou_ba_intra; User Id=sa;Password=")
        dbConnLocal.Open(StrConnBa)

        StrSql = "SELECT * FROM SYNCD_SincDettaglio WHERE SYNCD_CodContenuto= " & SYNCD_CodContenuto
        RsTemp = dbConnLocal.Execute(StrSql)

        txtResults.Text = txtResults.Text & "UPLOAD FILE XML PER APP CODICE:" & SYNCD_CodContenuto & " - START" & vbCrLf & vbCrLf

        While Not RsTemp.EOF
            StrTemp = Replace(RsTemp("SYNCD_FileOrigine").Value.ToString, StrPathUrlXml, "")
            Try

                'objFtp.UploadFile(txtPath.Text, txtLocalPath.Text, txtFileName.Text)
                'objFtp.UploadFile("/businessarea.flou.it/wwwroot/cl/apps/xml/", "C:\Website\tmpvb\", "simo_prova.xml")
                objFtp.UploadFile(StrPathFtpUpload, "C:\Website\tmpvb\", StrTemp.ToString)

                txtResults.Text = txtResults.Text & "UPLOAD " & StrPathFtpUpload & StrTemp.ToString & " | FATTO" & vbCrLf & vbCrLf

            Catch ex As WebException
                If ex.Status = WebExceptionStatus.Timeout Then
                    '''' code
                End If
                txtResults.Text = txtResults.Text & "UPLOAD " & StrPathFtpUpload & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
            Catch ex As Exception

                txtResults.Text = txtResults.Text & "UPLOAD " & StrPathFtpUpload & StrTemp.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
            End Try
            RsTemp.MoveNext()
        End While

        txtResults.Text = txtResults.Text & "FINE UPLOAD FILE XML PER APP CODICE:" & SYNCD_CodContenuto & " - END" & vbCrLf & vbCrLf

    End Sub


    Private Sub cmdAzzeraBia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAzzeraBia.Click

        txtResults.Text = ""

        AzzeraBia()

        ScaricaFileXmlApp(820)

    End Sub

    Private Sub cmdAzzeraRiv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAzzeraRiv.Click

        txtResults.Text = ""

        AzzeraRiv()

        ScaricaFileXmlApp(830)

    End Sub

    Private Sub cmdImpostaAppBiaRiv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImpostaAppBiaRiv.Click

        txtResults.Text = ""
        CaricaFileXmlApp(820)
        CaricaFileXmlApp(830)

        ' to do caricare record in SYNCV_SincVersioneApp per rilasciare aggiornamento
    End Sub

    Private Sub cmdImpostaAppBia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImpostaAppBia.Click

        txtResults.Text = ""
        CaricaFileXmlApp(820)

        ' to do caricare record in SYNCV_SincVersioneApp per rilasciare aggiornamento

    End Sub

    Private Sub cmdImpostaAppRiv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImpostaAppRiv.Click

        txtResults.Text = ""
        CaricaFileXmlApp(830)

        ' to do caricare record in SYNCV_SincVersioneApp per rilasciare aggiornamento
    End Sub

    Private Sub cmdSelectLettiAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSelectLettiAll.Click

        For idx As Integer = 1 To ChkListElencoLetti.Items.Count - 1
            ChkListElencoLetti.SetItemCheckState(idx, CheckState.Checked)
        Next

    End Sub

    Private Sub cmdUnSelectLettiAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUnSelectLettiAll.Click

        For idx As Integer = 1 To ChkListElencoLetti.Items.Count - 1
            ChkListElencoLetti.SetItemCheckState(idx, CheckState.Unchecked)
        Next

    End Sub

    Sub CancellaImmagini(ByVal StrPath As String)
        Dim ArrListLine() As String
        Dim ArrNomeFile() As String
        Dim StrNomeFile As String
        Dim StrResult As String
        Dim StrResultLine As String
        Dim StrSql As String
        Dim RsTemp As New ADODB.Recordset


        StrResult = objFtp.ListDirectory(StrPath)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
        'txtResults.Text = StrResult

        ArrListLine = Split(StrResult, vbNewLine)

        For i = 0 To UBound(ArrListLine)

            StrResultLine = ArrListLine(i).ToString
            If StrResultLine <> "" Then
                If StrResultLine.Substring(0, 1) = "d" Then
                    'directory richiamo ricorsivamente la funzione
                    txtResults.Text = txtResults.Text & StrPath & StrResultLine.Substring(59) & "/" & vbCrLf & vbCrLf

                    ' If StrResultLine.Substring(59, 1) > "1" Then
                    CancellaImmagini(StrPath & StrResultLine.Substring(59) & "/")
                    'End If
                Else
                    'file 
                    'txtResults.Text = txtResults.Text & StrResultLine.Substring(59)
                    StrNomeFile = StrResultLine.Substring(59)
                    ArrNomeFile = Split(StrNomeFile, ".")

                    If LCase(ArrNomeFile(1)) = "png" Then

                        ArrNomeFile = Split(ArrNomeFile(0), "_")
                        Application.DoEvents()

                        StrSql = "SELECT * FROM dbo.SCL_ScegliIlTuoLetto WHERE SCL_Id = " & ArrNomeFile(0)

                        OpenConnLettiRead()
                        RsTemp = dbConnConfLettiRead.Execute(StrSql)
                        If RsTemp.BOF = True And RsTemp.EOF = True Then
                        Else
                            If RsTemp("SCL_Online2012").Value.ToString = "0" Then
                                'candello il file
                                Try
                                    objFtp.DeleteFile(StrPath, StrNomeFile)
                                    txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf
                                Catch ex As WebException
                                    txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                                End Try
                            Else

                                StrSql = "SELECT *  FROM dbo.RIV_Rivestimenti WHERE  (ce_colo = '" & ArrNomeFile(3) & "') and  (ce_cove = '" & ArrNomeFile(2) & "')  "
                                OpenConnLettiRead()
                                RsTemp = dbConnConfLettiRead.Execute(StrSql)
                                If RsTemp.BOF = True And RsTemp.EOF = True Then
                                Else
                                    If RsTemp("fl_prod").Value.ToString = False Then
                                        'candello il file
                                        Try
                                            objFtp.DeleteFile(StrPath, StrNomeFile)
                                            txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf
                                        Catch ex As WebException
                                            txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                                        End Try
                                    Else


                                    End If
                                End If

                            End If
                        End If
                    Else
                        If LCase(StrNomeFile) = "thumbs.db" Then
                            Try
                                objFtp.DeleteFile(StrPath, StrNomeFile)
                                txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf
                            Catch ex As WebException
                                txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                            End Try
                        End If
                    End If
                End If

            End If


        Next

        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & "FINITO" & vbCrLf

    End Sub

    Private Sub btnDelFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelFile.Click
        CancellaImmagini("/www.flou.it/wwwroot2012/cl/media/r/riv2/")
    End Sub

    Private Sub btnEliminaXmlListino_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminaXmlListino.Click
        CancellaXmlListiniVecchi("/businessarea.flou.it/wwwroot/c/l/xml/")
    End Sub

    Sub CancellaXmlListiniVecchi(ByVal StrPath As String)
        Dim ArrListLine() As String
        Dim ArrNomeFile() As String
        Dim StrNomeFile As String
        Dim StrResult As String
        Dim StrResultLine As String
        Dim StrSql As String
        Dim RsTemp As New ADODB.Recordset


        StrResult = objFtp.ListDirectory(StrPath)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
        'txtResults.Text = StrResult

        ArrListLine = Split(StrResult, vbNewLine)

        For i = 0 To UBound(ArrListLine)

            StrResultLine = ArrListLine(i).ToString
            If StrResultLine <> "" Then
                If StrResultLine.Substring(24, 5) = "<DIR>" Then
                    'directory richiamo ricorsivamente la funzione
                    txtResults.Text = txtResults.Text & StrPath & StrResultLine.Substring(39) & "/" & vbCrLf & vbCrLf

                    ' If StrResultLine.Substring(59, 1) > "1" Then
                    'CancellaImmagini(StrPath & StrResultLine.Substring(59) & "/")
                    'End If
                Else
                    'file 
                    'txtResults.Text = txtResults.Text & StrResultLine.Substring(59)
                    StrNomeFile = StrResultLine.Substring(39)
                    ArrNomeFile = Split(StrNomeFile, ".")
                    ArrNomeFile = Split(ArrNomeFile(0), "_")

                    If LCase(ArrNomeFile(0)) = "letto" Then
                        If IsNumeric(ArrNomeFile(2)) = True Then


                            Application.DoEvents()

                            StrSql = "SELECT *   FROM dbo.SCL_ScegliIlTuoLetto WHERE SCL_Id = " & ArrNomeFile(2)

                            OpenConnLettiRead()
                            RsTemp = dbConnConfLettiRead.Execute(StrSql)
                            If RsTemp.BOF = True And RsTemp.EOF = True Then
                            Else
                                If RsTemp("SCL_Online").Value.ToString = "0" Then
                                    'cancello il file TO DO VERIFICO DEVO SISTEMARE SE VISIBILE SOLO IN BA
                                    Try
                                        objFtp.DeleteFile(StrPath, StrNomeFile)
                                        txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf
                                    Catch ex As WebException
                                        txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                                    End Try
                                Else

                                    StrSql = "SELECT * FROM dbo.LSTP_ListiniAlPubblico WHERE  (LSTP_CodBaan = '" & ArrNomeFile(4) & "')  "
                                    OpenConnBa()
                                    RsTemp = dbConnBa.Execute(StrSql)
                                    If RsTemp.BOF = True And RsTemp.EOF = True Then
                                    Else
                                        If RsTemp("LSTP_Stato").Value = "-1" Then
                                            'candello il file
                                            Try
                                                objFtp.DeleteFile(StrPath, StrNomeFile)
                                                txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf
                                            Catch ex As WebException
                                                txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                                            End Try
                                        Else
                                            ' cancello dati per un listino prefissato
                                            If RsTemp("LSTP_CodBaan").Value = "652" Then
                                                'candello il file
                                                Try
                                                    objFtp.DeleteFile(StrPath, StrNomeFile)
                                                    txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf
                                                Catch ex As WebException
                                                    txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                                                End Try
                                            End If

                                        End If
                                    End If

                                End If
                            End If
                        Else
                            If LCase(StrNomeFile) = "thumbs.db" Then
                                Try
                                    objFtp.DeleteFile(StrPath, StrNomeFile)
                                    txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf
                                Catch ex As WebException
                                    txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                                End Try
                            End If
                        End If
                    End If
                End If

            End If


        Next

        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & "FINITO" & vbCrLf

    End Sub
    Sub CancellaXmlLetti_Conf2(ByVal StrPath As String, ByVal StrSuffisso As String, ByVal StrListino As String, ByVal SCL_ID As String, intVuotaFileCache As Integer, intTipoFiltro As Integer)
        Dim ArrListLine() As String
        Dim ArrNomeFile() As String
        Dim StrNomeFile As String
        Dim StrResult As String
        Dim StrResultLine As String
        Dim RsTemp As New ADODB.Recordset


        StrResult = objFtp.ListDirectory(StrPath)      ' CANCELLO IL FILE DALLA CARTELLA DI APPOGGIO PER LA APP
        'txtResults.Text = StrResult

        ArrListLine = Split(StrResult, vbNewLine)

        For i = 0 To UBound(ArrListLine)

            StrResultLine = ArrListLine(i).ToString
            If StrResultLine <> "" Then
                If StrResultLine.Substring(24, 5) = "<DIR>" Then
                    'directory richiamo ricorsivamente la funzione
                    txtResults.Text = txtResults.Text & StrPath & StrResultLine.Substring(39) & "/" & vbCrLf & vbCrLf

                    ' If StrResultLine.Substring(59, 1) > "1" Then
                    'CancellaImmagini(StrPath & StrResultLine.Substring(59) & "/")
                    'End If
                Else
                    'file 
                    'txtResults.Text = txtResults.Text & StrResultLine.Substring(59)
                    StrNomeFile = Trim(StrResultLine.Substring(StrResultLine.LastIndexOf(" "c)))
                    ArrNomeFile = Split(StrNomeFile, ".")
                    ArrNomeFile = Split(ArrNomeFile(0), "_")

                    If InStr(StrNomeFile, "json_bed") <> 0 And intVuotaFileCache = 1 Then
                        objFtp.DownloadFile(StrPath, "C:\Websites\json_bed\", StrNomeFile)
                        objFtp.DeleteFile(StrPath, StrNomeFile)
                    End If

                    Select Case intTipoFiltro
                        Case 0 ' filtro per listino
                            If InStr(StrNomeFile, StrListino) > 0 Then

                                objFtp.DeleteFile(StrPath, StrNomeFile)
                                txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf

                            End If

                        Case 1 ' filtro per suffisso prodotto

                            If LCase(ArrNomeFile(0)) = StrSuffisso Then
                                If InStr(StrNomeFile, SCL_ID) > 0 Then

                                    Try
                                        objFtp.DeleteFile(StrPath, StrNomeFile)
                                        txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf
                                    Catch ex As WebException
                                        txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                                    End Try


                                End If
                            End If

                        Case 2 ' solo nome file

                            If LCase(ArrNomeFile(0)) = StrSuffisso Then
                                Try
                                    objFtp.DeleteFile(StrPath, StrNomeFile)
                                    txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | FATTO" & vbCrLf & vbCrLf

                                Catch ex As WebException
                                    txtResults.Text = txtResults.Text & StrPath & StrNomeFile.ToString & " | " & ex.Message.ToString & vbCrLf & vbCrLf
                                End Try
                            End If

                    End Select


                End If

            End If


        Next

        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & "FINITO" & vbCrLf

    End Sub
    Sub GeneraFileRender()
        Dim RsLetti, RsBaanVarianti, RsTemp As New ADODB.Recordset
        Dim StrSql, StrTempCAM1, StrTempCAM2, StrTempCAM3, StrTempRangeRender As String
        Dim ArrSCL_Letto, ArrRangeRender As Array

        txtResults.Text = ""
        StrTempCAM1 = ""
        StrTempCAM2 = ""
        StrTempCAM3 = ""

        For Each SCL_Item In ChkListElencoLetti.CheckedItems
            ArrSCL_Letto = Split(SCL_Item, "_")

            OpenConnLettiRead()
            StrSql = "SELECT * FROM dbo.SCL_ScegliIlTuoLetto WHERE SCL_Id = " & ArrSCL_Letto(0).ToString
            RsLetti = dbConnConfLettiRead.Execute(StrSql)

            If RsLetti("GRT_id_1").Value.ToString = "" Then

            Else
                ArrRangeRender = Split(ArrFrameRender(RsLetti("GRT_id_1").Value.ToString), ",")

                If ArrRangeRender.Length > 20 Then
                    StrTempRangeRender = ""
                    For i = 0 To ArrRangeRender.Length - 1
                        If i Mod 20 = 0 And i <> 0 Then
                            StrTempRangeRender = StrTempRangeRender & "," & ArrRangeRender(i)
                            txtResults.Text = txtResults.Text & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C01 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam1_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf
                            StrTempCAM1 = StrTempCAM1 & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C01 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam1_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf

                            txtResults.Text = txtResults.Text & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C02 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam2_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf
                            StrTempCAM2 = StrTempCAM2 & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C02 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam2_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf

                            txtResults.Text = txtResults.Text & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C03 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam3_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf
                            StrTempCAM3 = StrTempCAM3 & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C03 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam3_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf

                            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf

                            StrTempRangeRender = ""
                        Else
                            If StrTempRangeRender = "" Then
                                StrTempRangeRender = StrTempRangeRender & ArrRangeRender(i)
                            Else
                                StrTempRangeRender = StrTempRangeRender & "," & ArrRangeRender(i)
                            End If

                        End If
                    Next

                    txtResults.Text = txtResults.Text & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C01 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam1_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf
                    StrTempCAM1 = StrTempCAM1 & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C01 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam1_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf

                    txtResults.Text = txtResults.Text & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C02 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam2_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf
                    StrTempCAM2 = StrTempCAM2 & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C02 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam2_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf

                    txtResults.Text = txtResults.Text & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C03 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam3_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf
                    StrTempCAM3 = StrTempCAM3 & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C03 -frames:" & StrTempRangeRender & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam3_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf

                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                    StrTempRangeRender = ""

                    txtResults.Text = StrTempCAM1 & StrTempCAM2 & StrTempCAM3

                Else
                    txtResults.Text = txtResults.Text & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C01 -frames:" & ArrFrameRender(RsLetti("GRT_id_1").Value.ToString) & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam1_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf
                    txtResults.Text = txtResults.Text & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C02 -frames:" & ArrFrameRender(RsLetti("GRT_id_1").Value.ToString) & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam2_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf
                    txtResults.Text = txtResults.Text & Chr(34) & "C:\Program Files\Autodesk\3ds Max 2016\3dsmaxcmd.exe" & Chr(34) & " -camera:C03 -frames:" & ArrFrameRender(RsLetti("GRT_id_1").Value.ToString) & " -gammaCorrection:1 -outputName:K:\FLOU\_RENDER__2016\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & ArrSCL_Letto(0).ToString & "_cam3_.tga " & Chr(34) & "K:\FLOU\_3D_BED\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".max" & Chr(34) & vbCrLf & vbCrLf
                    txtResults.Text = txtResults.Text & vbCrLf & vbCrLf
                End If

            End If

            WriteData("C:\Websites\Letti_Script\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & ".bat", txtResults.Text)
            WriteData("C:\Websites\Letti_Script\" & UCase(RsLetti("SCL_deeplink").Value.ToString) & "_elencoframe.bat", ArrFrameRenderSingoli(RsLetti("GRT_id_1").Value.ToString))

            txtResults.Text = ""
            StrTempCAM1 = ""
            StrTempCAM2 = ""
            StrTempCAM3 = ""
            dbConnConfLettiRead.Close()
        Next

        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & "PROCEDURA FINITA"

    End Sub
    Sub WriteData(StrFilePath As String, StrData As String)

        Dim FILE_NAME As String = StrFilePath

        If System.IO.File.Exists(FILE_NAME) = True Then

            Dim objWriter As New System.IO.StreamWriter(FILE_NAME)

            objWriter.Write(StrData)
            objWriter.Close()
            'MessageBox.Show("Text written to file")

        Else
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, False)

            objWriter.Write(StrData)
            objWriter.Close()

            ' MessageBox.Show("File Does Not Exist")

        End If
    End Sub

    Sub GeneraArrayFrameRender(IntCodicaFiltroTessuti As Integer)
        Dim RsLetti, RsBaanVarianti, RsTemp As New ADODB.Recordset
        Dim StrSql, StrVarGruppo, StrElencoFrame, StrElencoFrameSingoli As String
        Dim IntCorreggiErrori, IntCatRender, IntIndiceRender As Integer

        IntCorreggiErrori = 1
        txtResults.Text = ""
        StrVarGruppo = ""

        OpenConnLettiRead()

        StrSql = "SELECT * FROM dbo.GRT_GruppiTessuti ORDER BY GRT_id "
        RsTemp = dbConnConfLettiRead.Execute(StrSql)
        While Not RsTemp.EOF

            StrVarGruppo = RsTemp("VAL_Codice").Value.ToString

            StrSql = " SELECT distinct dbo.TES_tessuti.de_cove_img, dbo.RIV_Rivestimenti.ce_cove, dbo.RIV_Rivestimenti.ce_colo, dbo.ESTL_TextureRenderLetti.Cat_Render, dbo.ESTL_TextureRenderLetti.ESTT_iden, dbo.ESTL_TextureRenderLetti.TEXT_ID_REN"
            StrSql = StrSql & " From TES_tessuti INNER Join RIV_Rivestimenti On TES_tessuti.ce_cove = RIV_Rivestimenti.ce_cove INNER Join VAL_TES_Varianti_Tessuti On TES_tessuti.ce_cove = VAL_TES_Varianti_Tessuti.ce_cove INNER Join ESTL_TextureRenderLetti On RIV_Rivestimenti.ce_cove = ESTL_TextureRenderLetti.ce_cove And RIV_Rivestimenti.ce_colo = ESTL_TextureRenderLetti.ce_colo"
            StrSql = StrSql & " Where (dbo.VAL_TES_Varianti_Tessuti.VAL_Codice IN ('" & StrVarGruppo.ToString & "')) "
            StrSql = StrSql & "   And (dbo.RIV_Rivestimenti.RIV_RenderOnline >= 1) And (dbo.TES_tessuti.online >= 1) "
            StrSql = StrSql & " GROUP BY dbo.RIV_Rivestimenti.ce_cove, dbo.RIV_Rivestimenti.ce_colo, dbo.RIV_Rivestimenti.nu_orde, dbo.TES_tessuti.de_cove_img, dbo.ESTL_TextureRenderLetti.Cat_Render, dbo.ESTL_TextureRenderLetti.ESTT_iden, dbo.ESTL_TextureRenderLetti.TEXT_ID_REN "
            StrSql = StrSql & " HAVING(dbo.ESTL_TextureRenderLetti.ESTT_iden = " & IntCodicaFiltroTessuti & ")"
            StrSql = StrSql & " ORDER BY dbo.ESTL_TextureRenderLetti.Cat_Render, dbo.ESTL_TextureRenderLetti.TEXT_ID_REN"

            StrSql = " SELECT distinct dbo.TES_tessuti.de_cove_img, dbo.RIV_Rivestimenti.ce_cove, dbo.RIV_Rivestimenti.ce_colo, dbo.ESTL_TextureRenderLetti.Cat_Render, dbo.ESTL_TextureRenderLetti.ESTT_iden, dbo.ESTL_TextureRenderLetti.TEXT_ID_REN"
            StrSql = StrSql & " From TES_tessuti INNER Join RIV_Rivestimenti On TES_tessuti.ce_cove = RIV_Rivestimenti.ce_cove INNER Join VAL_TES_Varianti_Tessuti_Listino On TES_tessuti.ce_cove = VAL_TES_Varianti_Tessuti_Listino.ce_cove INNER Join ESTL_TextureRenderLetti On RIV_Rivestimenti.ce_cove = ESTL_TextureRenderLetti.ce_cove And RIV_Rivestimenti.ce_colo = ESTL_TextureRenderLetti.ce_colo"
            StrSql = StrSql & " Where (dbo.VAL_TES_Varianti_Tessuti_Listino.VAL_Codice IN ('" & StrVarGruppo.ToString & "')) "
            StrSql = StrSql & "   And (dbo.RIV_Rivestimenti.RIV_RenderOnline >= 1) And (dbo.TES_tessuti.online >= 1) "
            StrSql = StrSql & " GROUP BY dbo.RIV_Rivestimenti.ce_cove, dbo.RIV_Rivestimenti.ce_colo, dbo.RIV_Rivestimenti.nu_orde, dbo.TES_tessuti.de_cove_img, dbo.ESTL_TextureRenderLetti.Cat_Render, dbo.ESTL_TextureRenderLetti.ESTT_iden, dbo.ESTL_TextureRenderLetti.TEXT_ID_REN "
            StrSql = StrSql & " HAVING(dbo.ESTL_TextureRenderLetti.ESTT_iden = " & IntCodicaFiltroTessuti & ")"
            StrSql = StrSql & " ORDER BY dbo.ESTL_TextureRenderLetti.Cat_Render, dbo.ESTL_TextureRenderLetti.TEXT_ID_REN"
            RsLetti = dbConnConfLettiRead.Execute(StrSql)

            StrElencoFrame = ""
            StrElencoFrameSingoli = ""
            While Not RsLetti.EOF
                If StrElencoFrame = "" Then
                    IntCatRender = RsLetti("Cat_Render").Value
                    IntIndiceRender = RsLetti("TEXT_ID_REN").Value
                    'StrElencoFrame = Microsoft.VisualBasic.Right("00" & RsLetti("Cat_Render").Value, 2) & Microsoft.VisualBasic.Right("000" & RsLetti("TEXT_ID_REN").Value, 3)
                    StrElencoFrame = RsLetti("Cat_Render").Value & Microsoft.VisualBasic.Right("000" & RsLetti("TEXT_ID_REN").Value, 2)

                End If
                If IntCatRender = RsLetti("Cat_Render").Value Then
                    If IntIndiceRender = RsLetti("TEXT_ID_REN").Value Then

                    Else
                        If RsLetti("TEXT_ID_REN").Value - IntIndiceRender > 1 Then
                            StrElencoFrame = StrElencoFrame & "-" & RsLetti("Cat_Render").Value & Microsoft.VisualBasic.Right("000" & IntIndiceRender, 2) & "," & RsLetti("Cat_Render").Value & Microsoft.VisualBasic.Right("000" & RsLetti("TEXT_ID_REN").Value, 2)
                        End If
                    End If
                Else

                    StrElencoFrame = StrElencoFrame & "-" & IntCatRender & Microsoft.VisualBasic.Right("000" & IntIndiceRender, 2) & "," & RsLetti("Cat_Render").Value & Microsoft.VisualBasic.Right("000" & RsLetti("TEXT_ID_REN").Value, 2)
                End If

                IntCatRender = RsLetti("Cat_Render").Value
                IntIndiceRender = RsLetti("TEXT_ID_REN").Value

                'gestione elenco frame singoli
                If StrElencoFrameSingoli <> "" Then StrElencoFrameSingoli = StrElencoFrameSingoli & ","
                StrElencoFrameSingoli = StrElencoFrameSingoli & RsLetti("Cat_Render").Value & Microsoft.VisualBasic.Right("000" & RsLetti("TEXT_ID_REN").Value, 2)


                RsLetti.MoveNext()

                If RsLetti.EOF = True Then
                    StrElencoFrame = StrElencoFrame & "-" & IntCatRender & Microsoft.VisualBasic.Right("000" & IntIndiceRender, 2) & ""
                End If


            End While

            ArrFrameRender(RsTemp("GRT_id").Value.ToString) = StrElencoFrame
            ArrFrameRenderSingoli(RsTemp("GRT_id").Value.ToString) = StrElencoFrameSingoli
            txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & RsTemp("GRT_id").Value.ToString & "  -- " & ArrFrameRender(RsTemp("GRT_id").Value.ToString)
            RsTemp.MoveNext()
        End While

        dbConnConfLettiRead.Close()

        'txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & RsTemp("GRT_id").Value.ToString & "  -- " & ArrFrameRender(RsTemp("GRT_id").Value.ToString)
        txtResults.Text = txtResults.Text & vbCrLf & vbCrLf & "PROCEDURA FINITA"
    End Sub

    Private Sub CmdGeneraFileRender_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdGeneraFileRender.Click
        Dim IntCodicaFiltroTessuti As Integer   ' la varibile indica se filtrare per tutti i tessuti o solo le novita
        IntCodicaFiltroTessuti = 500

        GeneraArrayFrameRender(IntCodicaFiltroTessuti)
        GeneraFileRender()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        objFtp.UseSSL = 0

        objFtp.UserName = "flouftp"
        objFtp.Password = "vVq895rasR!"
        objFtp.Host = "217.29.160.141"


        ' CancellaXmlLetti_Conf2("/Flou/wwwroot/bedconfigurator/model/static/", "pgateway", "", "156", 1)
        ' CancellaXmlLetti_Conf2("/Flou/wwwroot/bedconfigurator/modelba/static/", "pgateway", "", "156", 1) 'pgateway

        ' PARAMETRO intTipoFiltro = 2 CANCELLO PER NOME DEL FILE nopn cancello i file json_bed per rendere piu veloce la procedura
        '  CancellaXmlLetti_Conf2("/Flou/wwwroot/bedconfigurator/model/static/", "structure", "", "", 0, 2)
        '  CancellaXmlLetti_Conf2("/Flou/wwwroot/bedconfigurator/modelba/static/", "structure", "", "", 0, 2) 'pgateway

        CancellaXmlLetti_Conf2("/Flou/www2021/public/bedconfigurator/model/static/", "pgateway", "", "", 0, 2)
        CancellaXmlLetti_Conf2("/Flou/www2021/public/bedconfigurator/modelba/static/", "pgateway", "", "", 0, 2) 'pgateway

        ' PARAMETRO intTipoFiltro = 1 CANCELLO PER LISTINO
        '  CancellaXmlLetti_Conf2("/Flou/wwwroot/bedconfigurator/model/static/", "", "651", "", 0, 0)
        '  CancellaXmlLetti_Conf2("/Flou/wwwroot/bedconfigurator/modelba/static/", "", "651", "", 0, 0)

        ' CancellaXmlLetti_Conf2("/Flou/wwwroot/bedconfigurator/modeldev/static/", "pgateway", "", "")

    End Sub
End Class